package mongo

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
)

var MongoUri string
var MongoName string
var client *mongo.Client
var collections = make(map[string]Collection)

func initClient() {
	if client != nil {
		return
	}
	MongoUri = os.Getenv("MONGO_URI")
	if MongoUri == "" {
		panic(errors.New("require env `MONGO_URI`"))
	}
	MongoName = os.Getenv("MONGO_NAME")
	if MongoName == "" {
		panic(errors.New("require env `MONGO_NAME`"))
	}
	// mongo connection
	var err error
	clientOptions := options.Client().ApplyURI(MongoUri)
	client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		panic(err)
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		fmt.Println("ping error", MongoUri)
		panic(err)
	}
	// mongo client listening
	log.Println("MongoDB:", MongoUri)
	log.Println("Connected to MongoDB!")
}

func InitCollection(name string) Collection {
	initClient()
	if collection, ok := collections[name]; ok {
		return collection
	} else {
		collections[name] = initCollection(client.Database(MongoName).Collection(name))
		return collections[name]
	}
}

func Stop() {
	if client != nil {
		err := client.Disconnect(context.Background())
		if err != nil {
			panic(err)
		}
		log.Println("Connection to MongoDB closed.")
	}
}
