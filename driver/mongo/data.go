package mongo

import (
	"io/ioutil"
	"os"
)

type DataMigrateService interface {
	Migrate(data interface{}) error
	LoadFile() ([]byte, error)
	GetItems() []interface{}
}

type DataMigrateFile struct {
	Path string
}

func (d DataMigrateFile) LoadFile() ([]byte, error) {
	// read file from path
	jsonFile, err := os.Open(d.Path)
	if err != nil {
		return nil, err
	}
	defer jsonFile.Close()

	return ioutil.ReadAll(jsonFile)
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func NewDataMigrateFile(path string) DataMigrateFile {
	// validate path exist
	if !fileExists(path) {
		panic("file not found")
	}
	return DataMigrateFile{Path: path}
}
