package mongo

import "context"

type ServiceIndex interface {
	CreateIndex(ctx context.Context) ([]string, error)
}
