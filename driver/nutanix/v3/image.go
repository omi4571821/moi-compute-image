package v3

type NutanixService interface {
	GetImageList(limit int64) (*ImageNutanix, error)
}
