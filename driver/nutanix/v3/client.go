package v3

import (
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type HttpClient interface {
	Post(url string, payLoad string) ([]byte, error)
	AuthBasic()
}
type Client struct {
	User      string
	Pass      string
	AuthValue string
}

func (c Client) Post(url string, data string) ([]byte, error) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	var payload = strings.NewReader(data)
	req, err := http.NewRequest("POST", url, payload)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", c.AuthValue)
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)

}

func (c *Client) AuthBasic() {
	encodedString := base64.StdEncoding.EncodeToString([]byte(c.User + ":" + c.Pass))
	c.AuthValue = "Basic " + encodedString
}

type nutanixClient struct {
	IP   string
	Port string
	HttpClient
}

func (n nutanixClient) GetImageList(limit int64) (*ImageNutanix, error) {
	n.AuthBasic()
	baseUrl := n.GetBaseUrl()
	data, err := n.Post(baseUrl+"/images/list", fmt.Sprintf(`{"kind":"image","length":%d}`, limit))
	if err != nil {
		return nil, err
	}
	var imageNutanix *ImageNutanix
	err = json.Unmarshal(data, &imageNutanix)
	return imageNutanix, err
}

func (n nutanixClient) GetBaseUrl() string {
	return "https://" + n.IP + ":" + n.Port + "/api/nutanix/v3"
}

func NewService(user, pass, ip, port string) NutanixService {
	return &nutanixClient{
		HttpClient: &Client{
			User: user,
			Pass: pass,
		},
		IP:   ip,
		Port: port,
	}
}
