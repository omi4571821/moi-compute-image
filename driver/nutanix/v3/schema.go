package v3

import "time"

type Metadata struct {
	TotalMatches int    `json:"total_matches"`
	Kind         string `json:"kind"`
	Length       int    `json:"length"`
	Offset       int    `json:"offset"`
}

type Status struct {
	State       string     `json:"state"`
	Name        string     `json:"name"`
	Resources   *Resources `json:"resources"`
	Description string     `json:"description"`
}
type Resources struct {
	RetrievalUriList []string `json:"retrieval_uri_list"`
	ImageType        string   `json:"image_type"`
	SourceUri        string   `json:"source_uri"`
	Architecture     string   `json:"architecture"`
	SizeBytes        int      `json:"size_bytes"`
}

type Spec struct {
	Name        string     `json:"name"`
	Resources   *Resources `json:"resources"`
	Description string     `json:"description"`
}

type InternalMetaData struct {
	LastUpdateTime    time.Time   `json:"last_update_time"`
	Kind              string      `json:"kind"`
	UUID              string      `json:"uuid"`
	SpecVersion       int         `json:"spec_version"`
	CreationTime      time.Time   `json:"creation_time"`
	CategoriesMapping interface{} `json:"categories_mapping"`
	Categories        interface{} `json:"categories"`
}
type Entity struct {
	Status   *Status           `json:"status"`
	Spec     *Spec             `json:"spec"`
	Metadata *InternalMetaData `json:"metadata"`
}

type ImageNutanix struct {
	ApiVersion string    `json:"api_version"`
	Metadata   *Metadata `json:"metadata"`
	Entities   []*Entity `json:"entities"`
}
