FROM golang:1.19-alpine AS builder
WORKDIR /app
COPY . .

RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o www cmd/http/v1/main.go
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o tcp cmd/tcp/v1/main.go
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o sync cmd/job/sync/v1/main.go

FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /root/
COPY --from=builder /app/www .
COPY --from=builder /app/tcp .
COPY --from=builder /app/sync .
EXPOSE 8080
EXPOSE 9090
CMD ["./www"]