package v1

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"go.mongodb.org/mongo-driver/bson/primitive"
	repositoryBrokerMock "moi-compute-image/mock/src/repository/broker/v1"
	repositoryMock "moi-compute-image/mock/src/repository/v1"
	job "moi-compute-image/pkg/api/job/v1"
	broker "moi-compute-image/src/repository/broker/v1"
	repository "moi-compute-image/src/repository/v1"
	"reflect"
	"testing"
)

func Test_jobContext_Execute(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	mockBrokerServiceSuccess := repositoryBrokerMock.NewMockBrokerService(ctrl)
	brokerImages := []*broker.BrokerImage{
		{
			UUID:        "7f6e8e8e-3f1f-4f1f-8f1f-7f6e8e8e3f1f",
			Name:        "CentOS-7-x86_64-GenericCloud-2009.qcow2",
			Source:      "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-2009.qcow2",
			ImageType:   "DISK_IMAGE",
			Arch:        "x86_64",
			Status:      "ACTIVE",
			Description: "CentOS 7 x86_64 GenericCloud 2009",
		},
	}

	mockBrokerServiceSuccess.EXPECT().GetImageAll().Return(brokerImages, nil).AnyTimes()
	mockRepositoryManagementSuccess.EXPECT().GetByUUID(gomock.Any(), brokerImages[0].UUID).Return(nil, nil).AnyTimes()
	mockRepositoryManagementSuccess.EXPECT().CreateImage(gomock.Any(), brokerImages[0]).Return(nil).AnyTimes()
	// success ignore
	mockRepositoryManagementIgnore := repositoryMock.NewMockRepositoryManagement(ctrl)
	mockBrokerServiceIgnore := repositoryBrokerMock.NewMockBrokerService(ctrl)
	brokerImagesIgnore := []*broker.BrokerImage{
		{
			UUID:        "7f6e8e8e-3f1f-4f1f-8f1f-7f6e8e8e3f1f",
			Name:        "CentOS-7-x86_64-GenericCloud-2009.qcow2",
			Source:      "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-2009.qcow2",
			ImageType:   "DISK_IMAGE",
			Arch:        "x86_64",
			Status:      "COMPLETE",
			Description: "CentOS 7 x86_64 GenericCloud 2009",
		},
	}
	imageIgnoreSuccess := &repository.Image{
		Id:           primitive.NewObjectID(),
		UUID:         brokerImagesIgnore[0].UUID,
		Name:         brokerImagesIgnore[0].Name,
		Description:  brokerImagesIgnore[0].Description,
		IsEnabled:    false,
		SourceUri:    brokerImagesIgnore[0].Source,
		Architecture: "",
		DiskType:     repository.DISK_IMAGE,
		Icon:         nil,
		DisplayName:  "CentOS 7 x86_64 GenericCloud 2009",
	}
	mockBrokerServiceIgnore.EXPECT().GetImageAll().Return(brokerImagesIgnore, nil).AnyTimes()
	mockRepositoryManagementIgnore.EXPECT().GetByUUID(gomock.Any(), brokerImagesIgnore[0].UUID).Return(imageIgnoreSuccess, nil).AnyTimes()

	// fail
	mockRepositoryManagementFail := repositoryMock.NewMockRepositoryManagement(ctrl)
	mockBrokerServiceFail := repositoryBrokerMock.NewMockBrokerService(ctrl)
	mockBrokerServiceFail.EXPECT().GetImageAll().Return(nil, errors.New("Error")).AnyTimes()
	// fail create
	mockRepositoryManagementFailCreate := repositoryMock.NewMockRepositoryManagement(ctrl)
	mockBrokerServiceFailCreate := repositoryBrokerMock.NewMockBrokerService(ctrl)
	brokerImagesFailCreate := []*broker.BrokerImage{
		{
			UUID:        "7f6e8e8e-3f1f-4f1f-8f1f-7f6e8e8e3f1f",
			Name:        "CentOS-7-x86_64-GenericCloud-2009.qcow2",
			Source:      "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-2009.qcow2",
			ImageType:   "DISK_IMAGE",
			Arch:        "x86_64",
			Status:      "COMPLETE",
			Description: "CentOS 7 x86_64 GenericCloud 2009",
		},
	}
	mockBrokerServiceFailCreate.EXPECT().GetImageAll().Return(brokerImagesFailCreate, nil).AnyTimes()
	mockRepositoryManagementFailCreate.EXPECT().GetByUUID(gomock.Any(), brokerImagesFailCreate[0].UUID).Return(nil, nil).AnyTimes()
	mockRepositoryManagementFailCreate.EXPECT().CreateImage(gomock.Any(), brokerImagesFailCreate[0]).Return(errors.New("Error")).AnyTimes()

	type fields struct {
		JobUnImplement       *job.JobUnImplement
		RepositoryManagement repository.RepositoryManagement
		BrokerService        broker.BrokerService
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				JobUnImplement:       &job.JobUnImplement{},
				RepositoryManagement: mockRepositoryManagementSuccess,
				BrokerService:        mockBrokerServiceSuccess,
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: false,
		},
		{
			name: "fail",
			fields: fields{
				JobUnImplement:       &job.JobUnImplement{},
				RepositoryManagement: mockRepositoryManagementFail,
				BrokerService:        mockBrokerServiceFail,
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: true,
		},
		{
			name: "success ignore",
			fields: fields{
				JobUnImplement:       &job.JobUnImplement{},
				RepositoryManagement: mockRepositoryManagementIgnore,
				BrokerService:        mockBrokerServiceIgnore,
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: false,
		},
		{
			name: "fail create",
			fields: fields{
				JobUnImplement:       &job.JobUnImplement{},
				RepositoryManagement: mockRepositoryManagementFailCreate,
				BrokerService:        mockBrokerServiceFailCreate,
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := jobContext{
				JobUnImplement:       tt.fields.JobUnImplement,
				RepositoryManagement: tt.fields.RepositoryManagement,
				BrokerService:        tt.fields.BrokerService,
			}
			if err := j.Execute(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("Execute() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNewJobService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepositoryManagement := repositoryMock.NewMockRepositoryManagement(ctrl)
	mockBrokerService := repositoryBrokerMock.NewMockBrokerService(ctrl)

	type args struct {
		imageRepository repository.RepositoryManagement
		brokerService   broker.BrokerService
	}
	tests := []struct {
		name string
		args args
		want job.JobService
	}{
		{
			name: "success",
			args: args{
				imageRepository: mockRepositoryManagement,
				brokerService:   mockBrokerService,
			},
			want: &jobContext{
				JobUnImplement:       &job.JobUnImplement{},
				RepositoryManagement: mockRepositoryManagement,
				BrokerService:        mockBrokerService,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewJobService(tt.args.imageRepository, tt.args.brokerService); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewJobService() = %v, want %v", got, tt.want)
			}
		})
	}
}
