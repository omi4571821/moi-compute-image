package v1

import (
	"context"
	"log"
	job "moi-compute-image/pkg/api/job/v1"
	v1 "moi-compute-image/src/repository/broker/v1"
	repository "moi-compute-image/src/repository/v1"
)

type jobContext struct {
	*job.JobUnImplement
	repository.RepositoryManagement
	v1.BrokerService
}

func (j jobContext) Execute(ctx context.Context) error {
	images, err := j.BrokerService.GetImageAll()
	if err != nil {
		return err
	}
	for _, image := range images {
		instance, err := j.RepositoryManagement.GetByUUID(ctx, image.UUID)
		if err == nil && instance != nil {
			log.Println(instance.UUID, "already exist")
			continue
		}
		err = j.RepositoryManagement.CreateImage(ctx, image)
		if err != nil {
			return err
		}
	}
	return nil
}

func NewJobService(imageRepository repository.RepositoryManagement, brokerService v1.BrokerService) job.JobService {
	return &jobContext{
		JobUnImplement:       &job.JobUnImplement{},
		RepositoryManagement: imageRepository,
		BrokerService:        brokerService,
	}
}
