package v1

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"go.mongodb.org/mongo-driver/bson/primitive"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	repositoryMock "moi-compute-image/mock/src/repository/v1"
	"moi-compute-image/pkg/api/management/v1"
	protocol "moi-compute-image/pkg/protocol/v1"
	v1 "moi-compute-image/src/repository/v1"
	"reflect"
	"testing"
)

func Test_imageContext_ListImage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	inSuccess := &management.ImageListRequest{
		Page:   &management.Page{Page: 1, PerPage: 10},
		Filter: &management.ImageFilter{IsEnabled: true},
	}
	dataSuccess := &v1.ImagePage{
		Images: []*v1.Image{
			{
				Id:           primitive.NewObjectID(),
				UUID:         "1c3a8c80-1994-4b89-b223-8eb66e1258a9",
				Name:         "karbon-ntnx-1.0",
				Description:  "Karbon host OS version 1.0",
				IsEnabled:    true,
				SourceUri:    "https://ntnx-portal.s3.amazonaws.com/karbon/centos/ntnx-1.0/centos7-1.0.qcow2",
				Architecture: "X86_64",
				DiskType:     v1.DISK_IMAGE,
				Icon:         nil,
				DisplayName:  "karbon-ntnx-1.0",
			},
		},
		PageInfo: &v1.Page{
			Page:    1,
			Total:   1,
			PerPage: 10,
		},
	}
	ctx := context.Background()
	mockRepositoryManagementSuccess.EXPECT().GetImageWithPageInfo(ctx, inSuccess).Return(dataSuccess, nil)
	var items []*management.ImageSpec
	items = append(items, &management.ImageSpec{
		Id:          dataSuccess.Images[0].Id.Hex(),
		Image:       dataSuccess.Images[0].Name,
		Description: dataSuccess.Images[0].Description,
		IsEnabled:   dataSuccess.Images[0].IsEnabled,
		DiskType:    management.DISK_TYPE(dataSuccess.Images[0].DiskType),
	})
	wantSuccess := &management.ImageList{
		Items: items,
		PageInfo: &management.PageInfo{
			Page:        dataSuccess.PageInfo.GetPage(),
			TotalPage:   dataSuccess.PageInfo.GetTotalPage(),
			PerPage:     dataSuccess.PageInfo.GetPerPage(),
			NextPage:    dataSuccess.PageInfo.GetNextPage(),
			PrevPage:    dataSuccess.PageInfo.GetPrevPage(),
			HasNextPage: dataSuccess.PageInfo.GetHasNextPage(),
			HasPrevPage: dataSuccess.PageInfo.GetHasPrevPage(),
		},
	}
	// fail
	mockRepositoryManagementFail := repositoryMock.NewMockRepositoryManagement(ctrl)
	inFail := &management.ImageListRequest{
		Page:   &management.Page{Page: 1, PerPage: 10},
		Filter: &management.ImageFilter{IsEnabled: true},
	}
	dataFail := &v1.ImagePage{}
	dataFail = nil
	mockRepositoryManagementFail.EXPECT().GetImageWithPageInfo(ctx, inFail).Return(dataFail, pkgMongo.ErrNoDocuments)
	// fail with invalid argument
	mockRepositoryManagementFailInvalidArgument := repositoryMock.NewMockRepositoryManagement(ctrl)
	inFailInvalidArgument := &management.ImageListRequest{
		Page: &management.Page{Page: 1, PerPage: 10},
	}
	dataFailInvalidArgument := &v1.ImagePage{}
	dataFailInvalidArgument = nil
	mockRepositoryManagementFailInvalidArgument.EXPECT().GetImageWithPageInfo(ctx, inFailInvalidArgument).Return(dataFailInvalidArgument, pkgMongo.ErrClientDisconnected)
	type fields struct {
		UnimplementedImageServiceServer management.UnimplementedImageServiceServer
		RepositoryManagement            v1.RepositoryManagement
	}
	type args struct {
		ctx context.Context
		in  *management.ImageListRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *management.ImageList
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementSuccess,
			},
			args: args{
				ctx: ctx,
				in:  inSuccess,
			},
			want:    wantSuccess,
			wantErr: false,
		},
		{
			name: "fail",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFail,
			},
			args: args{
				ctx: ctx,
				in:  inFail,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "fail with invalid argument",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFailInvalidArgument,
			},
			args: args{
				ctx: ctx,
				in:  inFailInvalidArgument,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageContext{
				UnimplementedImageServiceServer: tt.fields.UnimplementedImageServiceServer,
				RepositoryManagement:            tt.fields.RepositoryManagement,
			}
			got, err := i.ListImage(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("ListImage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListImage() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_imageContext_GetImage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	idSuccess := primitive.NewObjectID()
	inSuccess := &management.ImageGetRequest{
		Id: idSuccess.Hex(),
	}
	dataSuccess := &v1.Image{
		Id:           primitive.NewObjectID(),
		UUID:         "1c3a8c80-1994-4b89-b223-8eb66e1258a9",
		Name:         "karbon-ntnx-1.0",
		Description:  "Karbon host OS version 1.0",
		IsEnabled:    true,
		SourceUri:    "https://ntnx-portal.s3.amazonaws.com/karbon/centos/ntnx-1.0/centos7-1.0.qcow2",
		Architecture: "X86_64",
		DiskType:     v1.DISK_IMAGE,
		Icon:         nil,
		DisplayName:  "karbon-ntnx-1.0",
	}
	ctxSuccess := context.Background()
	mockRepositoryManagementSuccess.EXPECT().GetImageById(ctxSuccess, idSuccess.Hex()).Return(dataSuccess, nil)
	// fail
	mockRepositoryManagementFail := repositoryMock.NewMockRepositoryManagement(ctrl)
	idFail := primitive.NewObjectID()
	inFail := &management.ImageGetRequest{
		Id: idFail.Hex(),
	}
	dataFail := &v1.Image{}
	dataFail = nil
	mockRepositoryManagementFail.EXPECT().GetImageById(ctxSuccess, idFail.Hex()).Return(dataFail, pkgMongo.ErrNilDocument)
	// fail with invalid argument
	mockRepositoryManagementFailInvalidArgument := repositoryMock.NewMockRepositoryManagement(ctrl)
	idFailInvalidArgument := primitive.NewObjectID()
	inFailInvalidArgument := &management.ImageGetRequest{
		Id: idFailInvalidArgument.Hex(),
	}
	dataFailInvalidArgument := &v1.Image{}
	dataFailInvalidArgument = nil
	mockRepositoryManagementFailInvalidArgument.EXPECT().GetImageById(ctxSuccess, idFailInvalidArgument.Hex()).Return(dataFailInvalidArgument, pkgMongo.ErrClientDisconnected)
	type fields struct {
		UnimplementedImageServiceServer management.UnimplementedImageServiceServer
		RepositoryManagement            v1.RepositoryManagement
	}
	type args struct {
		ctx context.Context
		in  *management.ImageGetRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *management.ImageFullSpec
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
			want: &management.ImageFullSpec{
				Id:           dataSuccess.Id.Hex(),
				Name:         dataSuccess.Name,
				Description:  dataSuccess.Description,
				IsEnabled:    dataSuccess.IsEnabled,
				DiskType:     management.DISK_TYPE(dataSuccess.DiskType),
				Icon:         dataSuccess.Icon,
				Source:       dataSuccess.SourceUri,
				Architecture: dataSuccess.Architecture,
				DisplayName:  dataSuccess.DisplayName,
				Uuid:         dataSuccess.UUID,
			},
			wantErr: false,
		},
		{
			name: "fail",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFail,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inFail,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "fail with invalid argument",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFailInvalidArgument,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inFailInvalidArgument,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageContext{
				UnimplementedImageServiceServer: tt.fields.UnimplementedImageServiceServer,
				RepositoryManagement:            tt.fields.RepositoryManagement,
			}
			got, err := i.GetImage(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetImage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetImage() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_imageContext_EnableImage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	idSuccess := primitive.NewObjectID()
	inSuccess := &management.ImageEnableRequest{
		Id: idSuccess.Hex(),
	}
	ctxSuccess := context.Background()
	mockRepositoryManagementSuccess.EXPECT().EnableImage(ctxSuccess, idSuccess.Hex()).Return(nil)
	// fail
	mockRepositoryManagementFail := repositoryMock.NewMockRepositoryManagement(ctrl)
	idFail := primitive.NewObjectID()
	inFail := &management.ImageEnableRequest{
		Id: idFail.Hex(),
	}
	mockRepositoryManagementFail.EXPECT().EnableImage(ctxSuccess, idFail.Hex()).Return(pkgMongo.ErrNilDocument)
	type fields struct {
		UnimplementedImageServiceServer management.UnimplementedImageServiceServer
		RepositoryManagement            v1.RepositoryManagement
	}
	type args struct {
		ctx context.Context
		in  *management.ImageEnableRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *emptypb.Empty
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
			want:    &emptypb.Empty{},
			wantErr: false,
		},
		{
			name: "fail",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFail,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inFail,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageContext{
				UnimplementedImageServiceServer: tt.fields.UnimplementedImageServiceServer,
				RepositoryManagement:            tt.fields.RepositoryManagement,
			}
			got, err := i.EnableImage(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("EnableImage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EnableImage() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_imageContext_DisableImage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	idSuccess := primitive.NewObjectID()
	inSuccess := &management.ImageDisableRequest{
		Id: idSuccess.Hex(),
	}
	ctxSuccess := context.Background()
	mockRepositoryManagementSuccess.EXPECT().DisableImage(ctxSuccess, idSuccess.Hex()).Return(nil)
	// fail
	mockRepositoryManagementFail := repositoryMock.NewMockRepositoryManagement(ctrl)
	idFail := primitive.NewObjectID()
	inFail := &management.ImageDisableRequest{
		Id: idFail.Hex(),
	}
	mockRepositoryManagementFail.EXPECT().DisableImage(ctxSuccess, idFail.Hex()).Return(pkgMongo.ErrNilDocument)

	type fields struct {
		UnimplementedImageServiceServer management.UnimplementedImageServiceServer
		RepositoryManagement            v1.RepositoryManagement
	}
	type args struct {
		ctx context.Context
		in  *management.ImageDisableRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *emptypb.Empty
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
			want: &emptypb.Empty{},
		},
		{
			name: "fail",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFail,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inFail,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageContext{
				UnimplementedImageServiceServer: tt.fields.UnimplementedImageServiceServer,
				RepositoryManagement:            tt.fields.RepositoryManagement,
			}
			got, err := i.DisableImage(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("DisableImage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DisableImage() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_imageContext_UpdateIcon(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	idSuccess := primitive.NewObjectID()
	inSuccess := &management.ImageIcon{
		Id:    idSuccess.Hex(),
		Image: []byte("test"),
	}
	ctxSuccess := context.Background()
	mockRepositoryManagementSuccess.EXPECT().UpdateIcon(ctxSuccess, idSuccess.Hex(), inSuccess.GetImage()).Return(nil)
	// fail
	mockRepositoryManagementFail := repositoryMock.NewMockRepositoryManagement(ctrl)
	idFail := primitive.NewObjectID()
	inFail := &management.ImageIcon{
		Id:    idFail.Hex(),
		Image: []byte("test"),
	}
	mockRepositoryManagementFail.EXPECT().UpdateIcon(ctxSuccess, idFail.Hex(), inFail.GetImage()).Return(pkgMongo.ErrNilDocument)
	// fail with invalid max size
	mockRepositoryManagementFailInvalidMaxSize := repositoryMock.NewMockRepositoryManagement(ctrl)
	idFailInvalidMaxSize := primitive.NewObjectID()
	inFailInvalidMaxSize := &management.ImageIcon{
		Id:    idFailInvalidMaxSize.Hex(),
		Image: make([]byte, MaxSize+1),
	}

	type fields struct {
		UnimplementedImageServiceServer management.UnimplementedImageServiceServer
		RepositoryManagement            v1.RepositoryManagement
	}
	type args struct {
		ctx context.Context
		in  *management.ImageIcon
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *emptypb.Empty
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
			want: &emptypb.Empty{},
		},
		{
			name: "fail",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFail,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inFail,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "fail with invalid max size",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFailInvalidMaxSize,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inFailInvalidMaxSize,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageContext{
				UnimplementedImageServiceServer: tt.fields.UnimplementedImageServiceServer,
				RepositoryManagement:            tt.fields.RepositoryManagement,
			}
			got, err := i.UpdateIcon(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateIcon() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateIcon() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_imageContext_ChangeDisplayName(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	idSuccess := primitive.NewObjectID()
	inSuccess := &management.ImageName{
		Id:   idSuccess.Hex(),
		Name: "karbon-ntnx-1.0",
	}
	ctxSuccess := context.Background()
	dataSuccess := &v1.Image{
		Id:           primitive.NewObjectID(),
		UUID:         "1c3a8c80-1994-4b89-b223-8eb66e1258a9",
		Name:         "karbon-ntnx-1.0",
		Description:  "Karbon host OS version 1.0",
		IsEnabled:    true,
		SourceUri:    "https://ntnx-portal.s3.amazonaws.com/karbon/centos/ntnx-1.0/centos7-1.0.qcow2",
		Architecture: "X86_64",
		DiskType:     v1.DISK_IMAGE,
		Icon:         nil,
	}
	dataSuccess.DisplayName = inSuccess.GetName()
	mockRepositoryManagementSuccess.EXPECT().GetImageById(ctxSuccess, idSuccess.Hex()).Return(dataSuccess, nil)
	mockRepositoryManagementSuccess.EXPECT().UpdateImage(ctxSuccess, dataSuccess).Return(nil)
	wantSuccess := &management.ImageFullSpec{
		Id:           dataSuccess.Id.Hex(),
		Name:         dataSuccess.Name,
		Description:  dataSuccess.Description,
		IsEnabled:    dataSuccess.IsEnabled,
		DiskType:     management.DISK_TYPE(dataSuccess.DiskType),
		Icon:         dataSuccess.Icon,
		Source:       dataSuccess.SourceUri,
		Architecture: dataSuccess.Architecture,
		DisplayName:  dataSuccess.GetDisplayName(),
		Uuid:         dataSuccess.UUID,
	}
	// fail
	mockRepositoryManagementFail := repositoryMock.NewMockRepositoryManagement(ctrl)
	idFail := primitive.NewObjectID()
	inFail := &management.ImageName{
		Id:   idFail.Hex(),
		Name: "karbon-ntnx-1.0",
	}
	ctxFail := context.Background()
	dataFail := &v1.Image{}
	dataFail = nil
	mockRepositoryManagementFail.EXPECT().GetImageById(ctxFail, idFail.Hex()).Return(dataFail, pkgMongo.ErrNilDocument)
	// fail with invalid argument
	mockRepositoryManagementFailInvalidArgument := repositoryMock.NewMockRepositoryManagement(ctrl)
	idFailInvalidArgument := primitive.NewObjectID()
	inFailInvalidArgument := &management.ImageName{
		Id:   idFailInvalidArgument.Hex(),
		Name: "karbon-ntnx-1.0",
	}
	ctxFailInvalidArgument := context.Background()
	dataFailInvalidArgument := &v1.Image{}
	dataFailInvalidArgument = nil
	mockRepositoryManagementFailInvalidArgument.EXPECT().GetImageById(ctxFailInvalidArgument, idFailInvalidArgument.Hex()).Return(dataFailInvalidArgument, pkgMongo.ErrClientDisconnected)
	// fail with update image
	mockRepositoryManagementFailUpdateImage := repositoryMock.NewMockRepositoryManagement(ctrl)
	idFailUpdateImage := primitive.NewObjectID()
	inFailUpdateImage := &management.ImageName{
		Id:   idFailUpdateImage.Hex(),
		Name: "karbon-ntnx-1.0",
	}
	ctxFailUpdateImage := context.Background()
	dataFailUpdateImage := &v1.Image{
		Id:           primitive.NewObjectID(),
		UUID:         "1c3a8c80-1994-4b89-b223-8eb66e1258a9",
		Name:         "karbon-ntnx-1.0",
		Description:  "Karbon host OS version 1.0",
		IsEnabled:    true,
		SourceUri:    "https://ntnx-portal.s3.amazonaws.com/karbon/centos/ntnx-1.0/centos7-1.0.qcow2",
		Architecture: "X86_64",
		DiskType:     v1.DISK_IMAGE,
		Icon:         nil,
	}
	dataFailUpdateImage.DisplayName = inFailUpdateImage.GetName()
	mockRepositoryManagementFailUpdateImage.EXPECT().GetImageById(ctxFailUpdateImage, idFailUpdateImage.Hex()).Return(dataFailUpdateImage, nil)
	mockRepositoryManagementFailUpdateImage.EXPECT().UpdateImage(ctxFailUpdateImage, dataFailUpdateImage).Return(pkgMongo.ErrNilDocument)
	type fields struct {
		UnimplementedImageServiceServer management.UnimplementedImageServiceServer
		RepositoryManagement            v1.RepositoryManagement
	}
	type args struct {
		ctx context.Context
		in  *management.ImageName
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *management.ImageFullSpec
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
			want: wantSuccess,
		},
		{
			name: "fail",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFail,
			},
			args: args{
				ctx: ctxFail,
				in:  inFail,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "fail with invalid argument",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFailInvalidArgument,
			},
			args: args{
				ctx: ctxFailInvalidArgument,
				in:  inFailInvalidArgument,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "fail with update image",
			fields: fields{
				RepositoryManagement: mockRepositoryManagementFailUpdateImage,
			},
			args: args{
				ctx: ctxFailUpdateImage,
				in:  inFailUpdateImage,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageContext{
				UnimplementedImageServiceServer: tt.fields.UnimplementedImageServiceServer,
				RepositoryManagement:            tt.fields.RepositoryManagement,
			}
			got, err := i.ChangeDisplayName(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("ChangeDisplayName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ChangeDisplayName() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_imageService_TcpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	serviceServer := &imageContext{
		RepositoryManagement: mockRepositoryManagementSuccess,
	}
	ctx := context.Background()
	type fields struct {
		ImageServiceServer management.ImageServiceServer
		Context            context.Context
	}
	type args struct {
		server *grpc.Server
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "success",
			fields: fields{
				ImageServiceServer: serviceServer,
				Context:            ctx,
			},
			args: args{
				server: grpc.NewServer(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageService{
				ImageServiceServer: tt.fields.ImageServiceServer,
				Context:            tt.fields.Context,
			}
			i.TcpListening(tt.args.server)
		})
	}
}

func Test_imageService_HttpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	serviceServer := &imageContext{
		RepositoryManagement: mockRepositoryManagementSuccess,
	}
	type fields struct {
		ImageServiceServer management.ImageServiceServer
		Context            context.Context
	}
	type args struct {
		mux  *runtime.ServeMux
		conn *grpc.ClientConn
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				ImageServiceServer: serviceServer,
			},
			args: args{
				mux:  runtime.NewServeMux(),
				conn: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageService{
				ImageServiceServer: tt.fields.ImageServiceServer,
				Context:            tt.fields.Context,
			}
			if err := i.HttpListening(tt.args.mux, tt.args.conn); (err != nil) != tt.wantErr {
				t.Errorf("HttpListening() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_iniManagementServiceServer(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	wantSuccess := &imageContext{
		RepositoryManagement: mockRepositoryManagementSuccess,
	}
	type args struct {
		image v1.RepositoryManagement
	}
	tests := []struct {
		name string
		args args
		want management.ImageServiceServer
	}{
		{
			name: "success",
			args: args{
				image: mockRepositoryManagementSuccess,
			},
			want: wantSuccess,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := iniManagementServiceServer(tt.args.image); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("iniManagementServiceServer() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestProtocol(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositoryManagementSuccess := repositoryMock.NewMockRepositoryManagement(ctrl)
	serverServiceSuccess := &imageContext{
		RepositoryManagement: mockRepositoryManagementSuccess,
	}
	ctx := context.Background()
	wantSuccess := &imageService{
		ImageServiceServer: serverServiceSuccess,
		Context:            ctx,
	}
	type args struct {
		ctx   context.Context
		image v1.RepositoryManagement
	}
	tests := []struct {
		name string
		args args
		want protocol.Service
	}{
		{
			name: "success",
			args: args{
				ctx:   ctx,
				image: mockRepositoryManagementSuccess,
			},
			want: wantSuccess,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Protocol(tt.args.ctx, tt.args.image); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Protocol() = %v, want %v", got, tt.want)
			}
		})
	}
}
