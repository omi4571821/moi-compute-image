package v1

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"moi-compute-image/pkg/api/management/v1"
	protocol "moi-compute-image/pkg/protocol/v1"
	v1 "moi-compute-image/src/repository/v1"
	"sync"
)

type imageContext struct {
	management.UnimplementedImageServiceServer
	v1.RepositoryManagement
}

const MaxSize = 50 * 1024 // 50KB

func (i imageContext) GetImageWithUUid(ctx context.Context, in *management.RequestUuid) (*management.ImageFullSpec, error) {
	data, err := i.RepositoryManagement.GetImageByUUId(ctx, in.GetUuid())
	if err != nil {
		if err == pkgMongo.ErrNilDocument {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &management.ImageFullSpec{
		Id:           data.Id.Hex(),
		Name:         data.Name,
		Description:  data.Description,
		IsEnabled:    data.IsEnabled,
		DiskType:     management.DISK_TYPE(data.DiskType),
		Icon:         data.Icon,
		Source:       data.SourceUri,
		Architecture: data.Architecture,
		DisplayName:  data.DisplayName,
		Uuid:         data.UUID,
	}, nil
}

func (i imageContext) ListImage(ctx context.Context, in *management.ImageListRequest) (*management.ImageList, error) {
	dataInfo, err := i.RepositoryManagement.GetImageWithPageInfo(ctx, in)
	if err != nil {
		if err == pkgMongo.ErrNoDocuments {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var wg sync.WaitGroup
	var images []*management.ImageSpec
	for _, data := range dataInfo.Images {
		wg.Add(1)
		go func(data *v1.Image) {
			defer wg.Done()
			images = append(images, &management.ImageSpec{
				Id:          data.Id.Hex(),
				Image:       data.Name,
				Description: data.Description,
				IsEnabled:   data.IsEnabled,
				DiskType:    management.DISK_TYPE(data.DiskType),
			})
		}(data)
	}
	wg.Wait()
	return &management.ImageList{
		Items: images,
		PageInfo: &management.PageInfo{
			Page:        dataInfo.PageInfo.GetPage(),
			TotalPage:   dataInfo.PageInfo.GetTotalPage(),
			PerPage:     dataInfo.PageInfo.GetPerPage(),
			NextPage:    dataInfo.PageInfo.GetNextPage(),
			PrevPage:    dataInfo.PageInfo.GetPrevPage(),
			HasNextPage: dataInfo.PageInfo.GetHasNextPage(),
			HasPrevPage: dataInfo.PageInfo.GetHasPrevPage(),
		},
	}, nil
}
func (i imageContext) GetImage(ctx context.Context, in *management.ImageGetRequest) (*management.ImageFullSpec, error) {
	data, err := i.RepositoryManagement.GetImageById(ctx, in.Id)
	if err != nil {
		if err == pkgMongo.ErrNilDocument {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &management.ImageFullSpec{
		Id:           data.Id.Hex(),
		Name:         data.Name,
		Description:  data.Description,
		IsEnabled:    data.IsEnabled,
		DiskType:     management.DISK_TYPE(data.DiskType),
		Icon:         data.Icon,
		Source:       data.SourceUri,
		Architecture: data.Architecture,
		DisplayName:  data.DisplayName,
		Uuid:         data.UUID,
	}, nil
}
func (i imageContext) EnableImage(ctx context.Context, in *management.ImageEnableRequest) (*emptypb.Empty, error) {
	err := i.RepositoryManagement.EnableImage(ctx, in.Id)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &emptypb.Empty{}, nil
}
func (i imageContext) DisableImage(ctx context.Context, in *management.ImageDisableRequest) (*emptypb.Empty, error) {
	err := i.RepositoryManagement.DisableImage(ctx, in.Id)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &emptypb.Empty{}, nil
}
func (i imageContext) UpdateIcon(ctx context.Context, in *management.ImageIcon) (*emptypb.Empty, error) {
	// TODO: check size
	if len(in.GetImage()) > MaxSize {
		return nil, status.Errorf(codes.InvalidArgument, "image size is too large")
	}
	err := i.RepositoryManagement.UpdateIcon(ctx, in.GetId(), in.GetImage())
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &emptypb.Empty{}, err
}
func (i imageContext) ChangeDisplayName(ctx context.Context, in *management.ImageName) (*management.ImageFullSpec, error) {
	data, err := i.RepositoryManagement.GetImageById(ctx, in.Id)
	if err != nil {
		if err == pkgMongo.ErrNilDocument {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	data.DisplayName = in.GetName()
	err = i.RepositoryManagement.UpdateImage(ctx, data)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &management.ImageFullSpec{
		Id:           data.Id.Hex(),
		Name:         data.Name,
		Description:  data.Description,
		IsEnabled:    data.IsEnabled,
		DiskType:     management.DISK_TYPE(data.DiskType),
		Icon:         data.Icon,
		Source:       data.SourceUri,
		Architecture: data.Architecture,
		DisplayName:  in.GetName(),
		Uuid:         data.UUID,
	}, nil
}

type imageService struct {
	management.ImageServiceServer
	context.Context
}

func (i imageService) TcpListening(server *grpc.Server) {
	management.RegisterImageServiceServer(server, i.ImageServiceServer)
}

func (i imageService) HttpListening(mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	return management.RegisterImageServiceHandler(i.Context, mux, conn)
}

func iniManagementServiceServer(image v1.RepositoryManagement) management.ImageServiceServer {
	return &imageContext{
		RepositoryManagement: image,
	}
}

func Protocol(ctx context.Context, image v1.RepositoryManagement) protocol.Service {
	return &imageService{
		ImageServiceServer: iniManagementServiceServer(image),
		Context:            ctx,
	}
}
