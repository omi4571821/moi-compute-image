package v1

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"moi-compute-image/pkg/api/image/v1"
	protocol "moi-compute-image/pkg/protocol/v1"
	repository "moi-compute-image/src/repository/v1"
)

type imageContext struct {
	image.UnimplementedImageDisplayServiceServer
	repository.Repository
}

func (i imageContext) GetImageList(ctx context.Context, in *empty.Empty) (*image.ImageList, error) {
	images, err := i.Repository.GetImagesByEnabled(ctx)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	var items []*image.Image
	for _, data := range images {
		items = append(items, &image.Image{
			Id:          data.Id.Hex(),
			DisplayName: data.DisplayName,
			Description: data.Description,
			Icon:        data.Icon,
			Uuid:        data.UUID,
			ProcessType: string(data.ProcessType),
			Template:    data.Template,
		})
	}
	imageList := &image.ImageList{
		Images: items,
	}
	return imageList, nil
}

type imageService struct {
	image.ImageDisplayServiceServer
	context.Context
}

func (i imageService) TcpListening(server *grpc.Server) {
	image.RegisterImageDisplayServiceServer(server, i.ImageDisplayServiceServer)
}

func (i imageService) HttpListening(mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	return image.RegisterImageDisplayServiceHandler(i.Context, mux, conn)
}

func initService(publisher repository.Repository) image.ImageDisplayServiceServer {
	return &imageContext{
		Repository: publisher,
	}
}

func NewImageService(ctx context.Context, publisher repository.Repository) protocol.Service {
	return &imageService{
		initService(publisher),
		ctx,
	}
}
