package v1

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
	repositoryMock "moi-compute-image/mock/src/repository/v1"
	"moi-compute-image/pkg/api/image/v1"
	protocol "moi-compute-image/pkg/protocol/v1"
	v1 "moi-compute-image/src/repository/v1"
	"reflect"
	"testing"
)

func Test_imageContext_GetImageList(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositorySuccess := repositoryMock.NewMockRepository(ctrl)
	images := []*v1.Image{
		{
			Id:           primitive.NewObjectID(),
			DisplayName:  "CentOS 7 x86_64 GenericCloud 2009",
			Description:  "CentOS 7 x86_64 GenericCloud 2009",
			IsEnabled:    true,
			SourceUri:    "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-2009.qcow2",
			Architecture: "",
			UUID:         "c2f1f8a0-6a6e-4a8b-8c5a-2a5a1d1c0c2a",
			DiskType:     v1.DISK_IMAGE,
			ProcessType:  v1.AUTO,
			Template:     "ubuntu",
		},
	}
	mockRepositorySuccess.EXPECT().GetImagesByEnabled(gomock.Any()).Return(images, nil).AnyTimes()
	var items []*image.Image
	inSuccess := &empty.Empty{}
	for _, data := range images {
		items = append(items, &image.Image{
			Id:          data.Id.Hex(),
			DisplayName: data.DisplayName,
			Description: data.Description,
			Icon:        data.Icon,
			Uuid:        data.UUID,
			ProcessType: string(data.ProcessType),
			Template:    data.Template,
		})
	}
	wantSuccess := &image.ImageList{
		Images: items,
	}
	// fail
	mockRepositoryFail := repositoryMock.NewMockRepository(ctrl)
	mockRepositoryFail.EXPECT().GetImagesByEnabled(gomock.Any()).Return(nil, errors.New("error")).AnyTimes()
	inFail := &empty.Empty{}
	wantFail := &image.ImageList{}
	wantFail = nil
	type fields struct {
		UnimplementedImageDisplayServiceServer image.UnimplementedImageDisplayServiceServer
		Repository                             v1.Repository
	}
	type args struct {
		ctx context.Context
		in  *empty.Empty
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *image.ImageList
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedImageDisplayServiceServer: image.UnimplementedImageDisplayServiceServer{},
				Repository:                             mockRepositorySuccess,
			},
			args: args{
				ctx: context.Background(),
				in:  inSuccess,
			},
			want: wantSuccess,
		},
		{
			name: "fail",
			fields: fields{
				UnimplementedImageDisplayServiceServer: image.UnimplementedImageDisplayServiceServer{},
				Repository:                             mockRepositoryFail,
			},
			args: args{
				ctx: context.Background(),
				in:  inFail,
			},
			want:    wantFail,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageContext{
				UnimplementedImageDisplayServiceServer: tt.fields.UnimplementedImageDisplayServiceServer,
				Repository:                             tt.fields.Repository,
			}
			got, err := i.GetImageList(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetImageList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetImageList() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_imageService_TcpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositorySuccess := repositoryMock.NewMockRepository(ctrl)
	serviceServerSuccess := &imageContext{
		UnimplementedImageDisplayServiceServer: image.UnimplementedImageDisplayServiceServer{},
		Repository:                             mockRepositorySuccess,
	}
	ctxSuccess := context.Background()
	type fields struct {
		ImageDisplayServiceServer image.ImageDisplayServiceServer
		Context                   context.Context
	}
	type args struct {
		server *grpc.Server
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "success",
			fields: fields{
				ImageDisplayServiceServer: serviceServerSuccess,
				Context:                   ctxSuccess,
			},
			args: args{
				server: grpc.NewServer(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageService{
				ImageDisplayServiceServer: tt.fields.ImageDisplayServiceServer,
				Context:                   tt.fields.Context,
			}
			i.TcpListening(tt.args.server)
		})
	}
}

func Test_imageService_HttpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositorySuccess := repositoryMock.NewMockRepository(ctrl)
	serviceServerSuccess := &imageContext{
		UnimplementedImageDisplayServiceServer: image.UnimplementedImageDisplayServiceServer{},
		Repository:                             mockRepositorySuccess,
	}
	ctxSuccess := context.Background()

	type fields struct {
		ImageDisplayServiceServer image.ImageDisplayServiceServer
		Context                   context.Context
	}
	type args struct {
		mux  *runtime.ServeMux
		conn *grpc.ClientConn
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				ImageDisplayServiceServer: serviceServerSuccess,
				Context:                   ctxSuccess,
			},
			args: args{
				mux:  runtime.NewServeMux(),
				conn: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := imageService{
				ImageDisplayServiceServer: tt.fields.ImageDisplayServiceServer,
				Context:                   tt.fields.Context,
			}
			if err := i.HttpListening(tt.args.mux, tt.args.conn); (err != nil) != tt.wantErr {
				t.Errorf("HttpListening() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_initService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositorySuccess := repositoryMock.NewMockRepository(ctrl)
	serviceServerSuccess := &imageContext{
		UnimplementedImageDisplayServiceServer: image.UnimplementedImageDisplayServiceServer{},
		Repository:                             mockRepositorySuccess,
	}
	type args struct {
		publisher v1.Repository
	}
	tests := []struct {
		name string
		args args
		want image.ImageDisplayServiceServer
	}{
		{
			name: "success",
			args: args{
				publisher: mockRepositorySuccess,
			},
			want: serviceServerSuccess,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := initService(tt.args.publisher); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("initService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewImageService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockRepositorySuccess := repositoryMock.NewMockRepository(ctrl)
	serviceServerSuccess := &imageContext{
		UnimplementedImageDisplayServiceServer: image.UnimplementedImageDisplayServiceServer{},
		Repository:                             mockRepositorySuccess,
	}
	ctxSuccess := context.Background()

	type args struct {
		ctx       context.Context
		publisher v1.Repository
	}
	tests := []struct {
		name string
		args args
		want protocol.Service
	}{
		{
			name: "success",
			args: args{
				ctx:       ctxSuccess,
				publisher: mockRepositorySuccess,
			},
			want: &imageService{
				ImageDisplayServiceServer: serviceServerSuccess,
				Context:                   ctxSuccess,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewImageService(tt.args.ctx, tt.args.publisher); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewImageService() = %v, want %v", got, tt.want)
			}
		})
	}
}
