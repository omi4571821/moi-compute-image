package v1

import (
	driverV3 "moi-compute-image/driver/nutanix/v3"
)

type nutanixBroker struct {
	nutanixService driverV3.NutanixService
}

func (n nutanixBroker) GetImageAll() ([]*BrokerImage, error) {
	imgInfo, err := n.nutanixService.GetImageList(40)
	if err != nil {
		return nil, err
	}
	var images []*BrokerImage
	for _, img := range imgInfo.Entities {
		images = append(images, &BrokerImage{
			UUID:        img.Metadata.UUID,
			Name:        img.Spec.Name,
			Source:      img.Spec.Resources.SourceUri,
			ImageType:   img.Spec.Resources.ImageType,
			Arch:        img.Spec.Resources.Architecture,
			Status:      img.Status.State,
			Description: img.Spec.Description,
		})
	}
	return images, nil
}

func NutanixBroker(nutanixService driverV3.NutanixService) BrokerService {
	return &nutanixBroker{
		nutanixService: nutanixService,
	}
}
