package v1

import (
	"errors"
	"github.com/golang/mock/gomock"
	v3 "moi-compute-image/driver/nutanix/v3"
	mock_v3 "moi-compute-image/mock/driver/nutanix/v3"
	"reflect"
	"testing"
	"time"
)

func Test_nutanixBroker_GetImageAll(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockNutanixServiceSuccess := mock_v3.NewMockNutanixService(ctrl)
	dataSuccess := &v3.ImageNutanix{
		ApiVersion: "3.1.0",
		Entities: []*v3.Entity{
			{
				Status: &v3.Status{
					State: "COMPLETE",
					Resources: &v3.Resources{
						RetrievalUriList: []string{"https://127.0.0.1:9440/api/nutanix/v3/images/1c3a8c80-1994-4b89-b223-8eb66e1258a9/file"},
						ImageType:        "DISK_IMAGE",
						SourceUri:        "https://ntnx-portal.s3.amazonaws.com/karbon/centos/ntnx-1.0/centos7-1.0.qcow2",
						Architecture:     "X86_64",
						SizeBytes:        15728640000,
					},
					Name: "Karbon host OS version 1.0",
				},
				Spec: &v3.Spec{
					Name:        "karbon-ntnx-1.0",
					Description: "Karbon host OS version 1.0",
					Resources: &v3.Resources{
						RetrievalUriList: []string{},
						SourceUri:        "https://ntnx-portal.s3.amazonaws.com/karbon/centos/ntnx-1.0/centos7-1.0.qcow2",
						Architecture:     "X86_64",
					},
				},
				Metadata: &v3.InternalMetaData{
					Kind:              "image",
					UUID:              "1c3a8c80-1994-4b89-b223-8eb66e1258a9",
					SpecVersion:       0,
					CreationTime:      time.Now(),
					Categories:        nil,
					CategoriesMapping: nil,
				},
			},
		},
		Metadata: &v3.Metadata{
			Kind:         "image",
			TotalMatches: 1,
			Length:       1,
			Offset:       0,
		},
	}
	mockNutanixServiceSuccess.EXPECT().GetImageList(gomock.Any()).Return(dataSuccess, nil)
	var wantSuccess []*BrokerImage
	wantSuccess = append(wantSuccess, &BrokerImage{
		UUID:        dataSuccess.Entities[0].Metadata.UUID,
		Name:        dataSuccess.Entities[0].Spec.Name,
		Source:      dataSuccess.Entities[0].Spec.Resources.SourceUri,
		ImageType:   dataSuccess.Entities[0].Spec.Resources.ImageType,
		Arch:        dataSuccess.Entities[0].Spec.Resources.Architecture,
		Status:      dataSuccess.Entities[0].Status.State,
		Description: dataSuccess.Entities[0].Spec.Description,
	})
	// fail
	mockNutanixServiceFail := mock_v3.NewMockNutanixService(ctrl)
	mockNutanixServiceFail.EXPECT().GetImageList(gomock.Any()).Return(nil, errors.New("error"))
	type fields struct {
		nutanixService v3.NutanixService
	}
	tests := []struct {
		name    string
		fields  fields
		want    []*BrokerImage
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				nutanixService: mockNutanixServiceSuccess,
			},
			want: wantSuccess,
		},
		{
			name: "fail",
			fields: fields{
				nutanixService: mockNutanixServiceFail,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := nutanixBroker{
				nutanixService: tt.fields.nutanixService,
			}
			got, err := n.GetImageAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetImageAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetImageAll() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNutanixBroker(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	nutanixService := mock_v3.NewMockNutanixService(ctrl)
	type args struct {
		nutanixService v3.NutanixService
	}
	tests := []struct {
		name string
		args args
		want BrokerService
	}{
		{
			name: "success",
			args: args{
				nutanixService: nil,
			},
			want: &nutanixBroker{
				nutanixService: nil,
			},
		},
		{
			name: "fail",
			args: args{
				nutanixService: nutanixService,
			},
			want: &nutanixBroker{
				nutanixService: nutanixService,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NutanixBroker(tt.args.nutanixService); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NutanixBroker() = %v, want %v", got, tt.want)
			}
		})
	}
}
