package v1

type BrokerImage struct {
	UUID        string `json:"uuid"`
	Name        string `json:"name"`
	Source      string `json:"source"`
	ImageType   string `json:"image_type"`
	Arch        string `json:"arch"`
	Status      string `json:"status"`
	Description string `json:"description"`
}

type BrokerService interface {
	GetImageAll() ([]*BrokerImage, error)
}
