package v1

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DISK_TYPE int32
type PROCESS_TYPE string

const ISO_IMAGE DISK_TYPE = 0
const DISK_IMAGE DISK_TYPE = 1
const MANUAL PROCESS_TYPE = "manual"
const AUTO PROCESS_TYPE = "auto"
const SEMI_AUTO PROCESS_TYPE = "semi_auto"

type PageService interface {
	GetPage() int64
	GetPerPage() int64
	GetTotalPage() int64
	GetNextPage() int64
	GetPrevPage() int64
	GetHasNextPage() bool
	GetHasPrevPage() bool
	GetTotalDocs() int
}

type Image struct {
	Id           primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	UUID         string             `json:"uuid" bson:"uuid"`
	Name         string             `bson:"name,omitempty" json:"name,omitempty"`
	Description  string             `bson:"description,omitempty" json:"description,omitempty"`
	IsEnabled    bool               `bson:"is_enabled,omitempty" json:"is_enabled,omitempty"`
	SourceUri    string             `json:"source_uri" bson:"source_uri"`
	Architecture string             `json:"architecture" json:"architecture"`
	DiskType     DISK_TYPE          `bson:"disk_type,omitempty" json:"disk_type,omitempty"`
	Icon         []byte             `bson:"icon,omitempty" json:"icon,omitempty"`
	DisplayName  string             `bson:"display_name,omitempty" json:"display_name,omitempty"`
	Template     string             `bson:"template,omitempty" json:"template,omitempty"`
	ProcessType  PROCESS_TYPE       `bson:"process_type,omitempty" json:"process_type,omitempty"`
}

func (receiver Image) GetDisplayName() string {
	if receiver.DisplayName == "" {
		return receiver.Name
	}
	return receiver.DisplayName
}

type Page struct {
	Page    int64 `json:"page"`
	Total   int64 `json:"total"`
	PerPage int64 `json:"per_page"`
}

func (p Page) GetPage() int64 {
	if p.Page <= 0 {
		return 1
	}
	return p.Page
}

func (p Page) GetTotalDocs() int {
	return int(p.GetTotal())
}

func (p Page) GetPerPage() int64 {
	if p.PerPage <= 0 {
		return 10
	}
	return p.PerPage
}

func (p Page) GetTotalPage() int64 {
	if p.GetTotal() <= 0 {
		return 0
	}
	return (p.GetTotal() + p.GetPerPage() - 1) / p.GetPerPage()
}

func (p Page) GetNextPage() int64 {
	if p.GetPage() >= p.GetTotalPage() || p.GetTotalPage() <= 0 {
		return p.GetTotalPage()
	} else {
		return p.Page + 1
	}
}

func (p Page) GetPrevPage() int64 {
	if p.Total <= 0 || p.GetPage() <= 1 {
		return 0
	}

	if p.GetPage() >= p.GetTotalPage() {
		return p.GetTotalPage() - 1
	}
	return p.GetPage() - 1
}

func (p Page) GetHasNextPage() bool {
	return p.GetPage() < p.GetTotalPage()
}

func (p Page) GetHasPrevPage() bool {
	return p.GetPage() <= p.GetTotalPage()
}

func (p Page) GetTotal() int64 {
	if p.Total <= 0 {
		return 0
	}
	return p.Total
}

type ImagePage struct {
	PageInfo PageService
	Images   []*Image
}

type RepositoryManagement interface {
	GetImageWithPageInfo(ctx context.Context, in interface{}) (*ImagePage, error)
	GetImageById(ctx context.Context, id string) (*Image, error)
	EnableImage(ctx context.Context, id string) error
	DisableImage(ctx context.Context, id string) error
	UpdateIcon(ctx context.Context, id string, icon []byte) error
	CreateImage(ctx context.Context, image interface{}) error
	GetByUUID(ctx context.Context, uuid string) (*Image, error)
	UpdateImage(ctx context.Context, data *Image) error
	GetImageByUUId(ctx context.Context, uuid string) (*Image, error)
}

type Repository interface {
	GetImagesByEnabled(ctx context.Context) ([]*Image, error)
}
