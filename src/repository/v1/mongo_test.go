package v1

import (
	"context"
	"github.com/golang/mock/gomock"
	"go.mongodb.org/mongo-driver/bson/primitive"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"moi-compute-image/driver/mongo"
	mongoMock "moi-compute-image/mock/driver/mongo"
	"moi-compute-image/pkg/api/management/v1"
	broker "moi-compute-image/src/repository/broker/v1"
	"reflect"
	"testing"
)

func Test_mongoRepository_UpdateImage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)
	idSuccess := primitive.NewObjectID()
	imageSuccess := &Image{
		Id:           idSuccess,
		Name:         "centos7.iso",
		Description:  "centos7",
		IsEnabled:    true,
		SourceUri:    "http://localhost:8080/centos7.iso",
		Architecture: "x86_64",
		DiskType:     ISO_IMAGE,
	}
	ctxSuccess := context.Background()
	filter := primitive.M{"_id": idSuccess}
	mockCollectionSuccess.EXPECT().UpdateOne(ctxSuccess, filter, primitive.M{"$set": imageSuccess}).Return(nil, nil)
	// fail
	mockCollectionFail := mongoMock.NewMockCollection(ctrl)
	idFail := primitive.NewObjectID()
	imageFail := &Image{
		Id:           idFail,
		Name:         "centos7.iso",
		Description:  "centos7",
		IsEnabled:    true,
		SourceUri:    "http://localhost:8080/centos7.iso",
		Architecture: "x86_64",
		DiskType:     ISO_IMAGE,
	}
	ctxFail := context.Background()
	filterFail := primitive.M{"_id": idFail}
	mockCollectionFail.EXPECT().UpdateOne(ctxFail, filterFail, primitive.M{"$set": imageFail}).Return(nil, pkgMongo.ErrNoDocuments)
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx  context.Context
		data *Image
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx:  ctxSuccess,
				data: imageSuccess,
			},
			wantErr: false,
		},
		{
			name: "fail",
			fields: fields{
				Collection: mockCollectionFail,
			},
			args: args{
				ctx:  ctxFail,
				data: imageFail,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoRepository{
				Collection: tt.fields.Collection,
			}
			if err := m.UpdateImage(tt.args.ctx, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("UpdateImage() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_isUUID(t *testing.T) {
	type args struct {
		u string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "success",
			args: args{
				u: "f47ac10b-58cc-0372-8567-0e02b2c3d479",
			},
			want: true,
		},
		{
			name: "fail",
			args: args{
				u: "asd123",
			},
			want: false,
		},
		{
			name: "fail with empty",
			args: args{
				u: "",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isUUID(tt.args.u); got != tt.want {
				t.Errorf("isUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoRepository_GetImagesByEnabled(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)
	cursorResultDecoderSuccess := mongoMock.NewMockCursorResultDecoder(ctrl)
	itemsSuccess := []*Image{
		{
			Id:           primitive.NewObjectID(),
			Name:         "centos7.iso",
			Description:  "centos7",
			DisplayName:  "CentOS 7 Minimal",
			IsEnabled:    true,
			SourceUri:    "http://localhost:8080/centos7.iso",
			Architecture: "x86_64",
			DiskType:     ISO_IMAGE,
		},
		{
			Id:           primitive.NewObjectID(),
			Name:         "windows10.iso",
			Description:  "windows10",
			IsEnabled:    true,
			SourceUri:    "http://localhost:8080/windows10.iso",
			DisplayName:  "Windows 10 Pro",
			Architecture: "x86_64",
			DiskType:     ISO_IMAGE,
		},
		{
			Id:           primitive.NewObjectID(),
			Name:         "ubuntu20-04.qcow2",
			Description:  "Ubuntu 20.04",
			DisplayName:  "Ubuntu 20.04 LTS",
			IsEnabled:    true,
			SourceUri:    "http://localhost:8080/ubuntu20-04.qcow2",
			Architecture: "x86_64",
			DiskType:     DISK_IMAGE,
		},
	}
	ctxSuccess := context.Background()
	gomock.InOrder(
		mockCollectionSuccess.EXPECT().Find(ctxSuccess, primitive.M{"is_enabled": true}).Return(cursorResultDecoderSuccess, nil),
		cursorResultDecoderSuccess.EXPECT().All(ctxSuccess, gomock.Any()).DoAndReturn(func(ctx context.Context, result interface{}) error {
			reflect.ValueOf(result).Elem().Set(reflect.ValueOf(itemsSuccess))
			return nil
		}),
	)
	// fail with cursor nil
	mockCollectionFailCursorNil := mongoMock.NewMockCollection(ctrl)
	ctxFailCursorNil := context.Background()
	mockCollectionFailCursorNil.EXPECT().Find(ctxFailCursorNil, primitive.M{"is_enabled": true}).Return(nil, pkgMongo.ErrNilCursor)
	// fail with cursor all error
	mockCollectionFailCursorAllError := mongoMock.NewMockCollection(ctrl)
	cursorResultDecoderFailCursorAllError := mongoMock.NewMockCursorResultDecoder(ctrl)
	ctxFailCursorAllError := context.Background()
	gomock.InOrder(
		mockCollectionFailCursorAllError.EXPECT().Find(ctxFailCursorAllError, primitive.M{"is_enabled": true}).Return(cursorResultDecoderFailCursorAllError, nil),
		cursorResultDecoderFailCursorAllError.EXPECT().All(ctxFailCursorAllError, gomock.Any()).Return(pkgMongo.ErrEmptySlice),
	)

	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []*Image
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
			},
			want:    itemsSuccess,
			wantErr: false,
		},
		{
			name: "fail with cursor nil",
			fields: fields{
				Collection: mockCollectionFailCursorNil,
			},
			args: args{
				ctx: ctxFailCursorNil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "fail with cursor all error",
			fields: fields{
				Collection: mockCollectionFailCursorAllError,
			},
			args: args{
				ctx: ctxFailCursorAllError,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoRepository{
				Collection: tt.fields.Collection,
			}
			got, err := m.GetImagesByEnabled(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetImagesByEnabled() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetImagesByEnabled() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoRepository_GetByUUID(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)
	singleResultDecoderSuccess := mongoMock.NewMockSingleResultDecoder(ctrl)
	ctxSuccess := context.Background()
	uuidSuccess := "f47ac10b-58cc-0372-8567-0e02b2c3d479"
	itemSuccess := &Image{
		Id:           primitive.NewObjectID(),
		Name:         "centos7.iso",
		Description:  "centos7",
		DisplayName:  "CentOS 7 Minimal",
		IsEnabled:    true,
		SourceUri:    "http://localhost:8080/centos7.iso",
		Architecture: "x86_64",
		UUID:         uuidSuccess,
		DiskType:     ISO_IMAGE,
	}
	gomock.InOrder(
		mockCollectionSuccess.EXPECT().FindOne(ctxSuccess, primitive.M{"uuid": uuidSuccess}).Return(singleResultDecoderSuccess),
		singleResultDecoderSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(result interface{}) error {
			reflect.ValueOf(result).Elem().Set(reflect.ValueOf(itemSuccess))
			return nil
		}),
	)
	// fail with invalid uuid
	mockCollectionFailInvalidUUID := mongoMock.NewMockCollection(ctrl)
	ctxFailInvalidUUID := context.Background()
	uuidFailInvalidUUID := "asd123"
	// fail with not found
	mockCollectionFailNotFound := mongoMock.NewMockCollection(ctrl)
	singleResultDecoderFailNotFound := mongoMock.NewMockSingleResultDecoder(ctrl)
	ctxFailNotFound := context.Background()
	uuidFailNotFound := "f47ac10b-58cc-0372-8567-0e02b2c3d479"
	gomock.InOrder(
		mockCollectionFailNotFound.EXPECT().FindOne(ctxFailNotFound, primitive.M{"uuid": uuidFailNotFound}).Return(singleResultDecoderFailNotFound),
		singleResultDecoderFailNotFound.EXPECT().Decode(gomock.Any()).Return(pkgMongo.ErrNoDocuments),
	)
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx  context.Context
		uuid string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Image
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx:  ctxSuccess,
				uuid: uuidSuccess,
			},
			want: itemSuccess,
		},
		{
			name: "fail with invalid uuid",
			fields: fields{
				Collection: mockCollectionFailInvalidUUID,
			},
			args: args{
				ctx:  ctxFailInvalidUUID,
				uuid: uuidFailInvalidUUID,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "fail with not found",
			fields: fields{
				Collection: mockCollectionFailNotFound,
			},
			args: args{
				ctx:  ctxFailNotFound,
				uuid: uuidFailNotFound,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoRepository{
				Collection: tt.fields.Collection,
			}
			got, err := m.GetByUUID(tt.args.ctx, tt.args.uuid)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByUUID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByUUID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoRepository_CreateImage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)
	ctxSuccess := context.Background()
	imageSuccess := &broker.BrokerImage{
		UUID:        "f47ac10b-58cc-0372-8567-0e02b2c3d479",
		Name:        "centos7.iso",
		Description: "centos7",
		ImageType:   "ISO_IMAGE",
		Source:      "http://localhost:8080/centos7.iso",
		Arch:        "x86_64",
	}
	insertOneResultSuccess := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	dataInsertSuccess := &Image{
		UUID:         imageSuccess.UUID,
		Name:         imageSuccess.Name,
		Description:  imageSuccess.Description,
		IsEnabled:    false,
		SourceUri:    imageSuccess.Source,
		Architecture: imageSuccess.Arch,
		DiskType:     ISO_IMAGE,
		Icon:         nil,
		ProcessType:  MANUAL,
	}
	mockCollectionSuccess.EXPECT().InsertOne(ctxSuccess, dataInsertSuccess).Return(insertOneResultSuccess, nil)
	// success with disk image
	mockCollectionSuccessDiskImage := mongoMock.NewMockCollection(ctrl)
	ctxSuccessDiskImage := context.Background()
	imageSuccessDiskImage := &broker.BrokerImage{
		UUID:        "f47ac10b-58cc-0372-8567-0e02b2c3d479",
		Name:        "centos7.qcow2",
		Description: "centos7",
		ImageType:   "DISK_IMAGE",
		Source:      "http://localhost:8080/centos7.qcow2",
		Arch:        "x86_64",
	}
	insertOneResultSuccessDiskImage := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	dataInsertSuccessDiskImage := &Image{
		UUID:         imageSuccessDiskImage.UUID,
		Name:         imageSuccessDiskImage.Name,
		Description:  imageSuccessDiskImage.Description,
		IsEnabled:    false,
		SourceUri:    imageSuccessDiskImage.Source,
		Architecture: imageSuccessDiskImage.Arch,
		DiskType:     DISK_IMAGE,
		Icon:         nil,
		ProcessType:  MANUAL,
	}
	mockCollectionSuccessDiskImage.EXPECT().InsertOne(ctxSuccessDiskImage, dataInsertSuccessDiskImage).Return(insertOneResultSuccessDiskImage, nil)
	// fail with invalid uuid
	mockCollectionFailInvalidUUID := mongoMock.NewMockCollection(ctrl)
	ctxFailInvalidUUID := context.Background()
	imageFailInvalidUUID := &broker.BrokerImage{
		UUID:        "asd123",
		Name:        "centos7.iso",
		Description: "centos7",
		ImageType:   "ISO_IMAGE",
		Source:      "http://localhost:8080/centos7.iso",
		Arch:        "x86_64",
	}
	// fail with input type
	mockCollectionFailInputType := mongoMock.NewMockCollection(ctrl)
	ctxFailInputType := context.Background()
	imageFailInputType := "test"

	// fail with insert error
	mockCollectionFailInsertError := mongoMock.NewMockCollection(ctrl)
	ctxFailInsertError := context.Background()
	imageFailInsertError := &broker.BrokerImage{
		UUID:        "f47ac10b-58cc-0372-8567-0e02b2c3d479",
		Name:        "centos7.iso",
		Description: "centos7",
		ImageType:   "ISO_IMAGE",
		Source:      "http://localhost:8080/centos7.iso",
		Arch:        "x86_64",
	}
	insertOneResultFailInsertError := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	dataInsertFailInsertError := &Image{
		UUID:         imageFailInsertError.UUID,
		Name:         imageFailInsertError.Name,
		Description:  imageFailInsertError.Description,
		IsEnabled:    false,
		SourceUri:    imageFailInsertError.Source,
		Architecture: imageFailInsertError.Arch,
		DiskType:     ISO_IMAGE,
		Icon:         nil,
		ProcessType:  MANUAL,
	}
	insertOneResultFailInsertError = nil
	mockCollectionFailInsertError.EXPECT().InsertOne(ctxFailInsertError, dataInsertFailInsertError).Return(insertOneResultFailInsertError, pkgMongo.ErrNoDocuments)
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
		in  interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  imageSuccess,
			},
			wantErr: false,
		},
		{
			name: "success with disk image",
			fields: fields{
				Collection: mockCollectionSuccessDiskImage,
			},
			args: args{
				ctx: ctxSuccessDiskImage,
				in:  imageSuccessDiskImage,
			},
			wantErr: false,
		},
		{
			name: "fail with invalid uuid",
			fields: fields{
				Collection: mockCollectionFailInvalidUUID,
			},
			args: args{
				ctx: ctxFailInvalidUUID,
				in:  imageFailInvalidUUID,
			},
			wantErr: true,
		},
		{
			name: "fail with input type",
			fields: fields{
				Collection: mockCollectionFailInputType,
			},
			args: args{
				ctx: ctxFailInputType,
				in:  imageFailInputType,
			},
			wantErr: true,
		},
		{
			name: "fail with insert error",
			fields: fields{
				Collection: mockCollectionFailInsertError,
			},
			args: args{
				ctx: ctxFailInsertError,
				in:  imageFailInsertError,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoRepository{
				Collection: tt.fields.Collection,
			}
			if err := m.CreateImage(tt.args.ctx, tt.args.in); (err != nil) != tt.wantErr {
				t.Errorf("CreateImage() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_mongoRepository_GetImageWithPageInfo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)
	cursorResultDecoderSuccess := mongoMock.NewMockCursorResultDecoder(ctrl)
	ctxSuccess := context.Background()
	pageNumberSuccess := int64(1)
	pageSizeSuccess := int64(10)
	totalSuccess := int64(3)
	pageSuccess := &Page{
		Page:    pageNumberSuccess,
		PerPage: pageSizeSuccess,
	}
	itemsSuccess := []*Image{
		{
			Id:           primitive.NewObjectID(),
			Name:         "centos7.iso",
			Description:  "centos7",
			DisplayName:  "CentOS 7 Minimal",
			IsEnabled:    true,
			SourceUri:    "http://localhost:8080/centos7.iso",
			Architecture: "x86_64",
			DiskType:     ISO_IMAGE,
		},
		{
			Id:           primitive.NewObjectID(),
			Name:         "windows10.iso",
			Description:  "windows10",
			IsEnabled:    true,
			SourceUri:    "http://localhost:8080/windows10.iso",
			DisplayName:  "Windows 10 Pro",
			Architecture: "x86_64",
			DiskType:     ISO_IMAGE,
		},
		{
			Id:           primitive.NewObjectID(),
			Name:         "ubuntu20-04.qcow2",
			Description:  "Ubuntu 20.04",
			DisplayName:  "Ubuntu 20.04 LTS",
			IsEnabled:    true,
			SourceUri:    "http://localhost:8080/ubuntu20-04.qcow2",
			Architecture: "x86_64",
			DiskType:     DISK_IMAGE,
		},
	}
	inSuccess := &management.ImageListRequest{
		Page: &management.Page{
			Page:    pageNumberSuccess,
			PerPage: pageSizeSuccess,
		},
		Filter: &management.ImageFilter{
			IsEnabled: true,
		},
	}
	mockCollectionSuccess.EXPECT().CountDocuments(ctxSuccess, gomock.Any()).Return(totalSuccess, nil)
	optionsSuccess := options.Find().SetSkip((pageSuccess.GetPage() - 1) * pageSuccess.GetPerPage()).SetLimit(pageSuccess.GetPerPage()).SetSort(primitive.M{"name": -1})
	filterSuccess := primitive.M{
		"is_enabled": inSuccess.GetFilter().GetIsEnabled(),
	}
	gomock.InOrder(
		mockCollectionSuccess.EXPECT().Find(ctxSuccess, filterSuccess, optionsSuccess).Return(cursorResultDecoderSuccess, nil),
		cursorResultDecoderSuccess.EXPECT().All(ctxSuccess, gomock.Any()).DoAndReturn(func(ctx context.Context, result interface{}) error {
			reflect.ValueOf(result).Elem().Set(reflect.ValueOf(itemsSuccess))
			return nil
		}),
	)
	wantSuccess := &ImagePage{
		Images: itemsSuccess,
		PageInfo: &Page{
			Page:    pageNumberSuccess,
			PerPage: pageSizeSuccess,
			Total:   totalSuccess,
		},
	}
	// success with default page
	mockCollectionSuccessDefaultPage := mongoMock.NewMockCollection(ctrl)
	cursorResultDecoderSuccessDefaultPage := mongoMock.NewMockCursorResultDecoder(ctrl)
	ctxSuccessDefaultPage := context.Background()
	pageNumberSuccessDefaultPage := int64(1)
	pageSizeSuccessDefaultPage := int64(10)
	totalSuccessDefaultPage := int64(3)
	pageSuccessDefaultPage := &Page{
		Page:    pageNumberSuccessDefaultPage,
		PerPage: pageSizeSuccessDefaultPage,
	}
	mockCollectionSuccessDefaultPage.EXPECT().CountDocuments(ctxSuccessDefaultPage, gomock.Any()).Return(totalSuccessDefaultPage, nil)
	optionsSuccessDefaultPage := options.Find().SetSkip((pageSuccessDefaultPage.GetPage() - 1) * pageSuccessDefaultPage.GetPerPage()).SetLimit(pageSuccessDefaultPage.GetPerPage()).SetSort(primitive.M{"name": -1})
	filterSuccessDefaultPage := primitive.M{}
	gomock.InOrder(
		mockCollectionSuccessDefaultPage.EXPECT().Find(ctxSuccessDefaultPage, filterSuccessDefaultPage, optionsSuccessDefaultPage).Return(cursorResultDecoderSuccessDefaultPage, nil),
		cursorResultDecoderSuccessDefaultPage.EXPECT().All(ctxSuccessDefaultPage, gomock.Any()).DoAndReturn(func(ctx context.Context, result interface{}) error {
			reflect.ValueOf(result).Elem().Set(reflect.ValueOf(itemsSuccess))
			return nil
		}),
	)
	inSuccessDefaultPage := &management.ImageListRequest{
		Page:   nil,
		Filter: nil,
	}
	wantSuccessDefaultPage := &ImagePage{
		Images: itemsSuccess,
		PageInfo: &Page{
			Page:    pageNumberSuccessDefaultPage,
			PerPage: pageSizeSuccessDefaultPage,
			Total:   totalSuccessDefaultPage,
		},
	}
	// fail input type
	mockCollectionFailInputType := mongoMock.NewMockCollection(ctrl)
	ctxFailInputType := context.Background()
	inFailInputType := "test"
	// fail with count error
	mockCollectionFailCountError := mongoMock.NewMockCollection(ctrl)
	cursorResultDecoderFailCountError := mongoMock.NewMockCursorResultDecoder(ctrl)
	ctxFailCountError := context.Background()
	pageNumberFailCountError := int64(1)
	pageSizeFailCountError := int64(10)
	inFailCountError := &management.ImageListRequest{
		Page: &management.Page{
			Page:    pageNumberFailCountError,
			PerPage: pageSizeFailCountError,
		},
		Filter: &management.ImageFilter{
			IsEnabled: true,
		},
	}
	mockCollectionFailCountError.EXPECT().CountDocuments(ctxFailCountError, gomock.Any()).Return(int64(0), pkgMongo.ErrEmptySlice)
	gomock.InOrder(
		mockCollectionFailCountError.EXPECT().Find(ctxFailCountError, gomock.Any(), gomock.Any()).Return(cursorResultDecoderFailCountError, nil),
		cursorResultDecoderFailCountError.EXPECT().All(ctxFailCountError, gomock.Any()).DoAndReturn(func(ctx context.Context, result interface{}) error {
			reflect.ValueOf(result).Elem().Set(reflect.ValueOf(itemsSuccess))
			return nil
		}),
	)
	wantFailCountError := &ImagePage{}
	wantFailCountError = nil
	// fail with cursor nil
	mockCollectionFailCursorNil := mongoMock.NewMockCollection(ctrl)
	ctxFailCursorNil := context.Background()
	pageNumberFailCursorNil := int64(1)
	pageSizeFailCursorNil := int64(10)
	mockCollectionFailCursorNil.EXPECT().CountDocuments(ctxFailCursorNil, gomock.Any()).Return(totalSuccess, nil)
	mockCollectionFailCursorNil.EXPECT().Find(ctxFailCursorNil, gomock.Any(), gomock.Any()).Return(nil, pkgMongo.ErrNilCursor)
	inFailCursorNil := &management.ImageListRequest{
		Page: &management.Page{
			Page:    pageNumberFailCursorNil,
			PerPage: pageSizeFailCursorNil,
		},
		Filter: &management.ImageFilter{
			IsEnabled: true,
		},
	}
	wantFailCursorNil := &ImagePage{}
	wantFailCursorNil = nil
	// fail with cursor all error
	mockCollectionFailCursorAllError := mongoMock.NewMockCollection(ctrl)
	cursorResultDecoderFailCursorAllError := mongoMock.NewMockCursorResultDecoder(ctrl)
	ctxFailCursorAllError := context.Background()
	pageNumberFailCursorAllError := int64(1)
	pageSizeFailCursorAllError := int64(10)
	mockCollectionFailCursorAllError.EXPECT().CountDocuments(ctxFailCursorAllError, gomock.Any()).Return(totalSuccess, nil)
	gomock.InOrder(
		mockCollectionFailCursorAllError.EXPECT().Find(ctxFailCursorAllError, gomock.Any(), gomock.Any()).Return(cursorResultDecoderFailCursorAllError, nil),
		cursorResultDecoderFailCursorAllError.EXPECT().All(ctxFailCursorAllError, gomock.Any()).Return(pkgMongo.ErrEmptySlice),
	)
	inFailCursorAllError := &management.ImageListRequest{
		Page: &management.Page{
			Page:    pageNumberFailCursorAllError,
			PerPage: pageSizeFailCursorAllError,
		},
		Filter: &management.ImageFilter{
			IsEnabled: true,
		},
	}
	wantFailCursorAllError := &ImagePage{}
	wantFailCursorAllError = nil
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
		in  interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *ImagePage
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
			want:    wantSuccess,
			wantErr: false,
		},
		{
			name: "success with default page",
			fields: fields{
				Collection: mockCollectionSuccessDefaultPage,
			},
			args: args{
				ctx: ctxSuccessDefaultPage,
				in:  inSuccessDefaultPage,
			},
			want:    wantSuccessDefaultPage,
			wantErr: false,
		},
		{
			name: "fail input type",
			fields: fields{
				Collection: mockCollectionFailInputType,
			},
			args: args{
				ctx: ctxFailInputType,
				in:  inFailInputType,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "fail with count error",
			fields: fields{
				Collection: mockCollectionFailCountError,
			},
			args: args{
				ctx: ctxFailCountError,
				in:  inFailCountError,
			},
			want:    wantFailCountError,
			wantErr: true,
		},
		{
			name: "fail with cursor nil",
			fields: fields{
				Collection: mockCollectionFailCursorNil,
			},
			args: args{
				ctx: ctxFailCursorNil,
				in:  inFailCursorNil,
			},
			want:    wantFailCursorNil,
			wantErr: true,
		},
		{
			name: "fail with cursor all error",
			fields: fields{
				Collection: mockCollectionFailCursorAllError,
			},
			args: args{
				ctx: ctxFailCursorAllError,
				in:  inFailCursorAllError,
			},
			want:    wantFailCursorAllError,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoRepository{
				Collection: tt.fields.Collection,
			}
			got, err := m.GetImageWithPageInfo(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetImageWithPageInfo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetImageWithPageInfo() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoRepository_GetImageById(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)
	singleResultDecoderSuccess := mongoMock.NewMockSingleResultDecoder(ctrl)
	ctxSuccess := context.Background()
	idSuccess := primitive.NewObjectID()
	itemSuccess := &Image{
		Id:           idSuccess,
		Name:         "centos7.iso",
		Description:  "centos7",
		DisplayName:  "CentOS 7 Minimal",
		IsEnabled:    true,
		SourceUri:    "http://localhost:8080/centos7.iso",
		Architecture: "x86_64",
		DiskType:     ISO_IMAGE,
	}
	gomock.InOrder(
		mockCollectionSuccess.EXPECT().FindOne(ctxSuccess, primitive.M{"_id": idSuccess}).Return(singleResultDecoderSuccess),
		singleResultDecoderSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(result interface{}) error {
			reflect.ValueOf(result).Elem().Set(reflect.ValueOf(itemSuccess))
			return nil
		}),
	)
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
		id  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Image
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				id:  idSuccess.Hex(),
			},
			want: itemSuccess,
		},
		{
			name: "fail with invalid id",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				id:  "asd123",
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoRepository{
				Collection: tt.fields.Collection,
			}
			got, err := m.GetImageById(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetImageById() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetImageById() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoRepository_EnableImage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)
	ctxSuccess := context.Background()
	idSuccess := primitive.NewObjectID()
	filterSuccess := primitive.M{"_id": idSuccess}
	imageSuccess := primitive.M{"$set": primitive.M{"is_enabled": true}}
	updateResultSuccess := &pkgMongo.UpdateResult{
		MatchedCount:  1,
		ModifiedCount: 1,
	}
	mockCollectionSuccess.EXPECT().UpdateOne(ctxSuccess, filterSuccess, imageSuccess).Return(updateResultSuccess, nil)
	// fail
	mockCollectionFail := mongoMock.NewMockCollection(ctrl)
	ctxFail := context.Background()
	idFail := primitive.NewObjectID()
	filterFail := primitive.M{"_id": idFail}
	imageFail := primitive.M{"$set": primitive.M{"is_enabled": true}}
	updateResultFail := &pkgMongo.UpdateResult{
		MatchedCount: 0,
	}
	mockCollectionFail.EXPECT().UpdateOne(ctxFail, filterFail, imageFail).Return(updateResultFail, pkgMongo.ErrNoDocuments)

	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
		id  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				id:  idSuccess.Hex(),
			},
			wantErr: false,
		},
		{
			name: "fail",
			fields: fields{
				Collection: mockCollectionFail,
			},
			args: args{
				ctx: ctxFail,
				id:  idFail.Hex(),
			},
			wantErr: true,
		},
		{
			name: "fail with invalid id",
			fields: fields{
				Collection: mockCollectionFail,
			},
			args: args{
				ctx: ctxFail,
				id:  "asd123",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoRepository{
				Collection: tt.fields.Collection,
			}
			if err := m.EnableImage(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("EnableImage() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_mongoRepository_DisableImage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)
	ctxSuccess := context.Background()
	idSuccess := primitive.NewObjectID()
	filterSuccess := primitive.M{"_id": idSuccess}
	imageSuccess := primitive.M{"$set": primitive.M{"is_enabled": false}}
	updateResultSuccess := &pkgMongo.UpdateResult{
		MatchedCount:  1,
		ModifiedCount: 1,
	}
	mockCollectionSuccess.EXPECT().UpdateOne(ctxSuccess, filterSuccess, imageSuccess).Return(updateResultSuccess, nil)
	// fail
	mockCollectionFail := mongoMock.NewMockCollection(ctrl)
	ctxFail := context.Background()
	idFail := primitive.NewObjectID()
	filterFail := primitive.M{"_id": idFail}
	imageFail := primitive.M{"$set": primitive.M{"is_enabled": false}}
	updateResultFail := &pkgMongo.UpdateResult{
		MatchedCount: 0,
	}
	mockCollectionFail.EXPECT().UpdateOne(ctxFail, filterFail, imageFail).Return(updateResultFail, pkgMongo.ErrNoDocuments)
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
		id  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				id:  idSuccess.Hex(),
			},
			wantErr: false,
		},
		{
			name: "fail with invalid id",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				id:  "asd123",
			},
			wantErr: true,
		},
		{
			name: "fail",
			fields: fields{
				Collection: mockCollectionFail,
			},
			args: args{
				ctx: ctxFail,
				id:  idFail.Hex(),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoRepository{
				Collection: tt.fields.Collection,
			}
			if err := m.DisableImage(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("DisableImage() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_mongoRepository_UpdateIcon(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)
	ctxSuccess := context.Background()
	idSuccess := primitive.NewObjectID()
	filterSuccess := primitive.M{"_id": idSuccess}
	imageSuccess := primitive.M{"$set": primitive.M{"icon": []byte("icon")}}
	updateResultSuccess := &pkgMongo.UpdateResult{
		MatchedCount:  1,
		ModifiedCount: 1,
	}
	mockCollectionSuccess.EXPECT().UpdateOne(ctxSuccess, filterSuccess, imageSuccess).Return(updateResultSuccess, nil)
	// fail
	mockCollectionFail := mongoMock.NewMockCollection(ctrl)
	ctxFail := context.Background()
	idFail := primitive.NewObjectID()
	filterFail := primitive.M{"_id": idFail}
	imageFail := primitive.M{"$set": primitive.M{"icon": []byte("icon")}}
	updateResultFail := &pkgMongo.UpdateResult{
		MatchedCount: 0,
	}
	mockCollectionFail.EXPECT().UpdateOne(ctxFail, filterFail, imageFail).Return(updateResultFail, pkgMongo.ErrNoDocuments)
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx  context.Context
		id   string
		icon []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx:  ctxSuccess,
				id:   idSuccess.Hex(),
				icon: []byte("icon"),
			},
			wantErr: false,
		},
		{
			name: "fail with invalid id",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				id:  "asd123",
			},
			wantErr: true,
		},
		{
			name: "fail",
			fields: fields{
				Collection: mockCollectionFail,
			},
			args: args{
				ctx:  ctxFail,
				id:   idFail.Hex(),
				icon: []byte("icon"),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoRepository{
				Collection: tt.fields.Collection,
			}
			if err := m.UpdateIcon(tt.args.ctx, tt.args.id, tt.args.icon); (err != nil) != tt.wantErr {
				t.Errorf("UpdateIcon() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNewMongoRepositoryManagement(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)

	type args struct {
		collection mongo.Collection
	}
	tests := []struct {
		name string
		args args
		want RepositoryManagement
	}{
		{
			name: "success",
			args: args{
				collection: mockCollectionSuccess,
			},
			want: &mongoRepository{
				Collection: mockCollectionSuccess,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewMongoRepositoryManagement(tt.args.collection); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMongoRepositoryManagement() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewMongoRepository(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockCollectionSuccess := mongoMock.NewMockCollection(ctrl)
	type args struct {
		collection mongo.Collection
	}
	tests := []struct {
		name string
		args args
		want Repository
	}{
		{
			name: "success",
			args: args{
				collection: mockCollectionSuccess,
			},
			want: &mongoRepository{
				Collection: mockCollectionSuccess,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewMongoRepository(tt.args.collection); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMongoRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}
