package v1

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

func TestImage_GetDisplayName(t *testing.T) {
	type fields struct {
		Id           primitive.ObjectID
		UUID         string
		Name         string
		Description  string
		IsEnabled    bool
		SourceUri    string
		Architecture string
		DiskType     DISK_TYPE
		Icon         []byte
		DisplayName  string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "GetDisplayName",
			fields: fields{
				Id:          primitive.NewObjectID(),
				DisplayName: "ok",
			},
			want: "ok",
		},
		{
			name: "GetDisplayName with name",
			fields: fields{
				Id:   primitive.NewObjectID(),
				Name: "ok",
			},
			want: "ok",
		},
		{
			name: "GetDisplayName with empty",
			fields: fields{
				Id: primitive.NewObjectID(),
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			receiver := Image{
				Id:           tt.fields.Id,
				UUID:         tt.fields.UUID,
				Name:         tt.fields.Name,
				Description:  tt.fields.Description,
				IsEnabled:    tt.fields.IsEnabled,
				SourceUri:    tt.fields.SourceUri,
				Architecture: tt.fields.Architecture,
				DiskType:     tt.fields.DiskType,
				Icon:         tt.fields.Icon,
				DisplayName:  tt.fields.DisplayName,
			}
			if got := receiver.GetDisplayName(); got != tt.want {
				t.Errorf("GetDisplayName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPage_GetPage(t *testing.T) {
	type fields struct {
		Page    int64
		Total   int64
		PerPage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   int64
	}{
		{
			name: "GetPage",
			fields: fields{
				Page: 1,
			},
			want: 1,
		},
		{
			name: "GetPage with empty",
			fields: fields{
				Page: 0,
			},
			want: 1,
		},
		{
			name: "GetPage with negative",
			fields: fields{
				Page: -1,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Page{
				Page:    tt.fields.Page,
				Total:   tt.fields.Total,
				PerPage: tt.fields.PerPage,
			}
			if got := p.GetPage(); got != tt.want {
				t.Errorf("GetPage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPage_GetPerPage(t *testing.T) {
	type fields struct {
		Page    int64
		Total   int64
		PerPage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   int64
	}{
		{
			name: "GetPerPage",
			fields: fields{
				PerPage: 1,
			},
			want: 1,
		},
		{
			name: "GetPerPage with empty",
			fields: fields{
				PerPage: 0,
			},
			want: 10,
		},
		{
			name: "GetPerPage with negative",
			fields: fields{
				PerPage: -1,
			},
			want: 10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Page{
				Page:    tt.fields.Page,
				Total:   tt.fields.Total,
				PerPage: tt.fields.PerPage,
			}
			if got := p.GetPerPage(); got != tt.want {
				t.Errorf("GetPerPage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPage_GetTotalPage(t *testing.T) {
	type fields struct {
		Page    int64
		Total   int64
		PerPage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   int64
	}{
		{
			name: "GetTotalPage",
			fields: fields{
				Total: 10,
			},
			want: 1,
		},
		{
			name: "GetTotalPage with per page",
			fields: fields{
				Total:   33,
				PerPage: 10,
			},
			want: 4,
		},
		{
			name: "GetTotalPage with per page and page",
			fields: fields{
				Total:   144,
				PerPage: 12,
			},
			want: 12,
		},
		{
			name: "GetTotalPage with empty",
			fields: fields{
				Total: 0,
			},
			want: 0,
		},
		{
			name: "GetTotalPage with negative",
			fields: fields{
				Total: -1,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Page{
				Page:    tt.fields.Page,
				Total:   tt.fields.Total,
				PerPage: tt.fields.PerPage,
			}
			if got := p.GetTotalPage(); got != tt.want {
				t.Errorf("GetTotalPage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPage_GetNextPage(t *testing.T) {
	type fields struct {
		Page    int64
		Total   int64
		PerPage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   int64
	}{
		{
			name: "GetNextPage",
			fields: fields{
				Page:    1,
				Total:   12,
				PerPage: 10,
			},
			want: 2,
		},
		{
			name: "GetNextPage current page is last page",
			fields: fields{
				Page:    3,
				PerPage: 10,
				Total:   30,
			},
			want: 3,
		},
		{
			name: "GetNextPage with empty",
			fields: fields{
				Page: 0,
			},
			want: 0,
		},
		{
			name: "GetNextPage with negative",
			fields: fields{
				Page: -1,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Page{
				Page:    tt.fields.Page,
				Total:   tt.fields.Total,
				PerPage: tt.fields.PerPage,
			}
			if got := p.GetNextPage(); got != tt.want {
				t.Errorf("GetNextPage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPage_GetPrevPage(t *testing.T) {
	type fields struct {
		Page    int64
		Total   int64
		PerPage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   int64
	}{
		{
			name: "GetPrevPage",
			fields: fields{
				Page:  1,
				Total: 30,
			},
			want: 0,
		},
		{
			name: "GetPrevPage with current page is first page",
			fields: fields{
				Page:  2,
				Total: 30,
			},
			want: 1,
		},
		{
			name: "GetPrevPage with empty",
			fields: fields{
				Page: 0,
			},
			want: 0,
		},
		{
			name: "GetPrevPage with negative",
			fields: fields{
				Page: -1,
			},
			want: 0,
		},
		{
			name: "GetPrevPage with last page",
			fields: fields{
				Page:  3,
				Total: 30,
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Page{
				Page:    tt.fields.Page,
				Total:   tt.fields.Total,
				PerPage: tt.fields.PerPage,
			}
			if got := p.GetPrevPage(); got != tt.want {
				t.Errorf("GetPrevPage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPage_GetHasNextPage(t *testing.T) {
	type fields struct {
		Page    int64
		Total   int64
		PerPage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "GetHasNextPage",
			fields: fields{
				Page:  1,
				Total: 12,
			},
			want: true,
		},
		{
			name: "GetHasNextPage with empty",
			fields: fields{
				Page: 0,
			},
			want: false,
		},
		{
			name: "GetHasNextPage with negative",
			fields: fields{
				Page: -1,
			},
			want: false,
		},
		{
			name: "GetHasNextPage with last page",
			fields: fields{
				Page:  3,
				Total: 30,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Page{
				Page:    tt.fields.Page,
				Total:   tt.fields.Total,
				PerPage: tt.fields.PerPage,
			}
			if got := p.GetHasNextPage(); got != tt.want {
				t.Errorf("GetHasNextPage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPage_GetHasPrevPage(t *testing.T) {
	type fields struct {
		Page    int64
		Total   int64
		PerPage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "GetHasPrevPage",
			fields: fields{
				Page:  2,
				Total: 12,
			},
			want: true,
		},
		{
			name: "GetHasPrevPage with empty",
			fields: fields{
				Page: 0,
			},
			want: false,
		},
		{
			name: "GetHasPrevPage with negative",
			fields: fields{
				Page: -1,
			},
			want: false,
		},
		{
			name: "GetHasPrevPage with last page",
			fields: fields{
				Page:  3,
				Total: 30,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Page{
				Page:    tt.fields.Page,
				Total:   tt.fields.Total,
				PerPage: tt.fields.PerPage,
			}
			if got := p.GetHasPrevPage(); got != tt.want {
				t.Errorf("GetHasPrevPage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPage_GetTotal(t *testing.T) {
	type fields struct {
		Page    int64
		Total   int64
		PerPage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   int64
	}{
		{
			name: "GetTotal",
			fields: fields{
				Total: 10,
			},
			want: 10,
		},
		{
			name: "GetTotal with empty",
			fields: fields{
				Total: 0,
			},
			want: 0,
		},
		{
			name: "GetTotal with negative",
			fields: fields{
				Total: -1,
			},
			want: 0,
		},
		{
			name: "GetTotal with page",
			fields: fields{
				Total:   10,
				PerPage: 10,
			},
			want: 10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Page{
				Page:    tt.fields.Page,
				Total:   tt.fields.Total,
				PerPage: tt.fields.PerPage,
			}
			if got := p.GetTotal(); got != tt.want {
				t.Errorf("GetTotal() = %v, want %v", got, tt.want)
			}
		})
	}
}
