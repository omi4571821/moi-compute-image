package v1

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"moi-compute-image/driver/mongo"
	"moi-compute-image/pkg/api/management/v1"
	broker "moi-compute-image/src/repository/broker/v1"
	"sync"
)

const CollectionName = "compute_images"

var ErrInvalidArgument = fmt.Errorf("invalid argument")
var supportAuto = []string{
	"c8fb469a-939d-4e45-b5f9-d41b2928c757",
	"23a2a6ee-4336-4751-ae7e-8153d53dbfb1",
}

type mongoRepository struct {
	mongo.Collection
}

func (m mongoRepository) GetImageByUUId(ctx context.Context, uuid string) (*Image, error) {
	var image *Image
	err := m.FindOne(ctx, primitive.M{"uuid": uuid}).Decode(&image)
	return image, err
}

func isUUID(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}
func (m mongoRepository) UpdateImage(ctx context.Context, data *Image) error {
	_, err := m.UpdateOne(ctx, primitive.M{"_id": data.Id}, primitive.M{"$set": data})
	return err
}

func (m mongoRepository) GetImagesByEnabled(ctx context.Context) ([]*Image, error) {
	var images []*Image
	cursor, err := m.Find(ctx, primitive.M{"is_enabled": true})
	if err != nil {
		return nil, err
	}
	err = cursor.All(ctx, &images)
	return images, err
}

func (m mongoRepository) GetByUUID(ctx context.Context, uuid string) (*Image, error) {
	if !isUUID(uuid) {
		return nil, ErrInvalidArgument
	}
	image := &Image{}
	if err := m.FindOne(ctx, primitive.M{"uuid": uuid}).Decode(&image); err != nil {
		return nil, err
	}
	return image, nil
}

func (m mongoRepository) CreateImage(ctx context.Context, in interface{}) error {
	switch in.(type) {
	case *broker.BrokerImage:
		data := in.(*broker.BrokerImage)
		diskType := ISO_IMAGE
		if data.ImageType == "DISK_IMAGE" {
			diskType = DISK_IMAGE
		}
		if !isUUID(data.UUID) {
			return ErrInvalidArgument
		}
		image := &Image{
			UUID:         data.UUID,
			Name:         data.Name,
			Description:  data.Description,
			IsEnabled:    false,
			SourceUri:    data.Source,
			Architecture: data.Arch,
			DiskType:     diskType,
			ProcessType:  MANUAL,
			Icon:         nil,
		}
		for _, uuidAuto := range supportAuto {
			if uuidAuto == data.UUID {
				image.ProcessType = AUTO
				break
			}
		}
		_, err := m.InsertOne(ctx, image)
		return err
	default:
		return ErrInvalidArgument
	}
}

func (m mongoRepository) GetImageWithPageInfo(ctx context.Context, in interface{}) (*ImagePage, error) {
	switch in.(type) {
	case *management.ImageListRequest:
		data := in.(*management.ImageListRequest)
		var images []*Image
		var page, perPage, total int64
		if data.GetPage() != nil {
			page = data.GetPage().GetPage()
			perPage = data.GetPage().GetPerPage()
		} else {
			page = 1
			perPage = 10
		}
		skip := (page - 1) * perPage
		filter := primitive.M{}
		if data.GetFilter() != nil {
			filter = primitive.M{
				"is_enabled": data.GetFilter().GetIsEnabled(),
			}
		}

		var wg sync.WaitGroup
		var errFind, errCount error
		wg.Add(2)
		go func() {
			defer wg.Done()
			optionsFind := options.Find().SetLimit(perPage).SetSkip(skip).SetSort(primitive.M{"name": -1})
			cursor, err := m.Find(ctx, filter, optionsFind)
			if err != nil {
				errFind = err
				return
			}
			if err = cursor.All(ctx, &images); err != nil {
				errFind = err
				return
			}
		}()
		go func() {
			defer wg.Done()
			var err error
			total, err = m.CountDocuments(ctx, filter)
			errCount = err
		}()
		wg.Wait()
		if errFind != nil {
			return nil, errFind
		}
		if errCount != nil {
			return nil, errCount
		}
		return &ImagePage{
			PageInfo: &Page{
				Page:    page,
				Total:   total,
				PerPage: perPage,
			},
			Images: images,
		}, nil
	default:
		return nil, ErrInvalidArgument
	}
}

func (m mongoRepository) GetImageById(ctx context.Context, id string) (*Image, error) {
	if id, err := primitive.ObjectIDFromHex(id); err != nil {
		return nil, err
	} else {
		image := &Image{}
		err := m.FindOne(ctx, primitive.M{"_id": id}).Decode(&image)
		return image, err
	}
}

func (m mongoRepository) EnableImage(ctx context.Context, id string) error {
	if id, err := primitive.ObjectIDFromHex(id); err != nil {
		return err
	} else {
		_, err := m.UpdateOne(ctx, primitive.M{"_id": id}, primitive.M{"$set": primitive.M{"is_enabled": true}})
		return err
	}
}

func (m mongoRepository) DisableImage(ctx context.Context, id string) error {
	if id, err := primitive.ObjectIDFromHex(id); err != nil {
		return err
	} else {
		_, err := m.UpdateOne(ctx, primitive.M{"_id": id}, primitive.M{"$set": primitive.M{"is_enabled": false}})
		return err
	}
}

func (m mongoRepository) UpdateIcon(ctx context.Context, id string, icon []byte) error {
	if id, err := primitive.ObjectIDFromHex(id); err != nil {
		return err
	} else {
		_, err := m.UpdateOne(ctx, primitive.M{"_id": id}, primitive.M{"$set": primitive.M{"icon": icon}})
		return err
	}
}

func NewMongoRepositoryManagement(collection mongo.Collection) RepositoryManagement {
	return &mongoRepository{collection}
}

func NewMongoRepository(collection mongo.Collection) Repository {
	return &mongoRepository{collection}
}
