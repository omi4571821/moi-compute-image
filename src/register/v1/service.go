package v1

import (
	"context"
	mongoDriver "moi-compute-image/driver/mongo"
	protocol "moi-compute-image/pkg/protocol/v1"
	image "moi-compute-image/src/image/v1"
	management "moi-compute-image/src/management/service/v1"
	repository "moi-compute-image/src/repository/v1"
)

func GetServiceAll() []protocol.Service {
	collection := mongoDriver.InitCollection(repository.CollectionName)
	repositoryManagement := repository.NewMongoRepositoryManagement(collection)
	mongoRepository := repository.NewMongoRepository(collection)
	return []protocol.Service{
		management.Protocol(context.Background(), repositoryManagement),
		image.NewImageService(context.Background(), mongoRepository),
	}
}
