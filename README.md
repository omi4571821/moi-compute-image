# MOI Compute image 
## Description
This image is used to run MOI Compute. It is based on the [MOI Base image](https://www.nutanix.com/sg/products/nutanix-cloud-infrastructure)

## Usage
### Build
```
docker build -t moi-compute .
```
### Run
```
docker run -d --name moi-compute -p 8080:8080 moi-compute
```
### Test
```
curl http://localhost:8080
```
