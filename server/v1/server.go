package v1

import (
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"log"
	protocol "moi-compute-image/pkg/protocol/v1"
	"strings"
)

type serverListening interface {
	listening() error
	setPort(ports ...string)
	registerAppProtocol(app []protocol.Service)
}

func TcpListening(tcpPort string, apps ...protocol.Service) {
	tcpServer := initTcpServer()
	tcpServer.setPort(tcpPort)
	tcpServer.registerAppProtocol(apps)
	if err := tcpServer.listening(); err != nil {
		panic(err)
	}
}

func HttpListening(tcpPort, httpPort string, app ...protocol.Service) {
	var allowHeaders = []string{
		"x-consumer-username",
		"x-consumer-id",
		"x-consumer-custom-id",
		"x-consumer-groups",
		"x-consumer-acl",
		"authorization",
		"user-agent",
	}
	httpServer := initHttpServer(
		runtime.WithIncomingHeaderMatcher(func(key string) (string, bool) {
			for _, allowHeader := range allowHeaders {
				if strings.EqualFold(key, allowHeader) {
					return runtime.MetadataPrefix + key, true
				}
			}
			return "", false
		}))
	httpServer.setPort(httpPort, tcpPort)
	httpServer.registerAppProtocol(app)
	go func() {
		if err := httpServer.listening(); err != nil {
			log.Printf("%v\n", err)
		}

	}()
	TcpListening(tcpPort, app...)
}
