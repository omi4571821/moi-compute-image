package v1

import (
	"context"
	"google.golang.org/grpc"
	"log"
	protocol "moi-compute-image/pkg/protocol/v1"
	"net"
	"os"
	"os/signal"
	"time"
)

type tcpServer struct {
	port string
	ctx  context.Context
	opts []grpc.ServerOption
	apps []protocol.Service
}

func (t *tcpServer) registerAppProtocol(app []protocol.Service) {
	t.apps = append(t.apps, app...)
}

func (t *tcpServer) listening() error {
	listen, err := net.Listen("tcp", ":"+t.port)
	if err != nil {
		return err
	}
	server := grpc.NewServer(t.opts...)
	for _, app := range t.apps {
		app.TcpListening(server)
	}
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		// sig is a ^C, handle it
		for range c {
			log.Println("shutting down TCP...")
			_, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			server.GracefulStop()
		}

	}()
	log.Println("TCP listening on port: ", t.port)
	return server.Serve(listen)
}

func (t *tcpServer) setPort(port ...string) {
	t.port = port[0]
}

func initTcpServer(opts ...grpc.ServerOption) serverListening {
	return &tcpServer{
		ctx:  context.Background(),
		opts: opts,
	}
}
