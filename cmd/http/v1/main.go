package main

import (
	"context"
	mongoDriver "moi-compute-image/driver/mongo"
	server "moi-compute-image/server/v1"
	register "moi-compute-image/src/register/v1"
	"os"
	"os/signal"
	"time"
)

const tcpPorts = "9090"
const httpPorts = "8080"

func main() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, os.Kill)
	go func() {
		// sig is a ^C, handle it
		for range c {
			_, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			mongoDriver.Stop()
		}

	}()
	server.HttpListening(tcpPorts, httpPorts, register.GetServiceAll()...)
}
