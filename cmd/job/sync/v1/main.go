package main

import (
	"context"
	"log"
	mongoDriver "moi-compute-image/driver/mongo"
	v3 "moi-compute-image/driver/nutanix/v3"
	jobManagemet "moi-compute-image/src/management/job/v1"
	broker "moi-compute-image/src/repository/broker/v1"
	repository "moi-compute-image/src/repository/v1"
	"os"
)

var (
	NutanixUser = ""
	NutanixPass = ""
	NutanixIp   = ""
	NutanixPort = ""
)

func init() {
	NutanixUser = os.Getenv("NUTANIX_USER")
	NutanixPass = os.Getenv("NUTANIX_PASS")
	NutanixIp = os.Getenv("NUTANIX_IP")
	NutanixPort = os.Getenv("NUTANIX_PORT")
	if NutanixUser == "" {
		panic("NUTANIX_USER is empty")
	}
	if NutanixPass == "" {
		panic("NUTANIX_PASS is empty")
	}
	if NutanixIp == "" {
		panic("NUTANIX_IP is empty")
	}
	if NutanixPort == "" {
		panic("NUTANIX_PORT is empty")
	}
}

//	func checkIPConnection(ip string, port string) error {
//		address := fmt.Sprintf("%s:%d", ip, port)
//		conn, err := net.DialTimeout("tcp", address, 2*time.Second)
//		if err != nil {
//			return err
//		}
//		defer conn.Close()
//		fmt.Printf("Successfully connected to %s\n", address)
//		return nil
//	}
func main() {
	defer mongoDriver.Stop()
	collection := mongoDriver.InitCollection(repository.CollectionName)
	mongoRepository := repository.NewMongoRepositoryManagement(collection)
	nutanixService := v3.NewService(NutanixUser, NutanixPass, NutanixIp, NutanixPort)
	brokerService := broker.NutanixBroker(nutanixService)
	jobService := jobManagemet.NewJobService(mongoRepository, brokerService)
	err := jobService.Execute(context.Background())
	if err != nil {
		panic(err)
	}
	log.Println("Successfully sync image")
}
