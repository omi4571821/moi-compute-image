protoc --proto_path=api/proto/v1 --proto_path=third_party --go_out==plugins=grpc:pkg --go-grpc_out==plugins=grpc:pkg petstore-service.proto
protoc --proto_path=api/proto/v1 --proto_path=third_party --grpc-gateway_out=logtostderr=true:pkg \
  --grpc-gateway_opt=logtostderr=true \
  --grpc-gateway_opt generate_unbound_methods=true \
  petstore-service.proto
protoc --proto_path=api/proto/v1 --proto_path=third_party --openapiv2_out=logtostderr=true:api/swagger/v1 \
  --openapiv2_opt logtostderr=true \
  petstore-service.proto

