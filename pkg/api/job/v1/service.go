package v1

import (
	"context"
	"errors"
)

type JobService interface {
	Execute(ctx context.Context) error
	unImplement()
}

type JobUnImplement struct {
}

func (j JobUnImplement) Execute(ctx context.Context) error {
	//TODO implement me
	panic("implement me")
	return errors.New("implement me")
}

func (j JobUnImplement) unImplement() {
	//TODO implement me
	panic("implement me")
}
