package v1

import (
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
)

type Service interface {
	TcpListening(server *grpc.Server)
	HttpListening(mux *runtime.ServeMux, conn *grpc.ClientConn) error
}
