// Code generated by MockGen. DO NOT EDIT.
// Source: src/repository/v1/repository.go

// Package mock is a generated GoMock package.
package mock

import (
	context "context"
	v1 "moi-compute-image/src/repository/v1"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockPageService is a mock of PageService interface.
type MockPageService struct {
	ctrl     *gomock.Controller
	recorder *MockPageServiceMockRecorder
}

// MockPageServiceMockRecorder is the mock recorder for MockPageService.
type MockPageServiceMockRecorder struct {
	mock *MockPageService
}

// NewMockPageService creates a new mock instance.
func NewMockPageService(ctrl *gomock.Controller) *MockPageService {
	mock := &MockPageService{ctrl: ctrl}
	mock.recorder = &MockPageServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockPageService) EXPECT() *MockPageServiceMockRecorder {
	return m.recorder
}

// GetHasNextPage mocks base method.
func (m *MockPageService) GetHasNextPage() bool {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetHasNextPage")
	ret0, _ := ret[0].(bool)
	return ret0
}

// GetHasNextPage indicates an expected call of GetHasNextPage.
func (mr *MockPageServiceMockRecorder) GetHasNextPage() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetHasNextPage", reflect.TypeOf((*MockPageService)(nil).GetHasNextPage))
}

// GetHasPrevPage mocks base method.
func (m *MockPageService) GetHasPrevPage() bool {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetHasPrevPage")
	ret0, _ := ret[0].(bool)
	return ret0
}

// GetHasPrevPage indicates an expected call of GetHasPrevPage.
func (mr *MockPageServiceMockRecorder) GetHasPrevPage() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetHasPrevPage", reflect.TypeOf((*MockPageService)(nil).GetHasPrevPage))
}

// GetNextPage mocks base method.
func (m *MockPageService) GetNextPage() int64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetNextPage")
	ret0, _ := ret[0].(int64)
	return ret0
}

// GetNextPage indicates an expected call of GetNextPage.
func (mr *MockPageServiceMockRecorder) GetNextPage() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetNextPage", reflect.TypeOf((*MockPageService)(nil).GetNextPage))
}

// GetPage mocks base method.
func (m *MockPageService) GetPage() int64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetPage")
	ret0, _ := ret[0].(int64)
	return ret0
}

// GetPage indicates an expected call of GetPage.
func (mr *MockPageServiceMockRecorder) GetPage() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetPage", reflect.TypeOf((*MockPageService)(nil).GetPage))
}

// GetPerPage mocks base method.
func (m *MockPageService) GetPerPage() int64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetPerPage")
	ret0, _ := ret[0].(int64)
	return ret0
}

// GetPerPage indicates an expected call of GetPerPage.
func (mr *MockPageServiceMockRecorder) GetPerPage() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetPerPage", reflect.TypeOf((*MockPageService)(nil).GetPerPage))
}

// GetPrevPage mocks base method.
func (m *MockPageService) GetPrevPage() int64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetPrevPage")
	ret0, _ := ret[0].(int64)
	return ret0
}

// GetPrevPage indicates an expected call of GetPrevPage.
func (mr *MockPageServiceMockRecorder) GetPrevPage() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetPrevPage", reflect.TypeOf((*MockPageService)(nil).GetPrevPage))
}

// GetTotalDocs mocks base method.
func (m *MockPageService) GetTotalDocs() int {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetTotalDocs")
	ret0, _ := ret[0].(int)
	return ret0
}

// GetTotalDocs indicates an expected call of GetTotalDocs.
func (mr *MockPageServiceMockRecorder) GetTotalDocs() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetTotalDocs", reflect.TypeOf((*MockPageService)(nil).GetTotalDocs))
}

// GetTotalPage mocks base method.
func (m *MockPageService) GetTotalPage() int64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetTotalPage")
	ret0, _ := ret[0].(int64)
	return ret0
}

// GetTotalPage indicates an expected call of GetTotalPage.
func (mr *MockPageServiceMockRecorder) GetTotalPage() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetTotalPage", reflect.TypeOf((*MockPageService)(nil).GetTotalPage))
}

// MockRepositoryManagement is a mock of RepositoryManagement interface.
type MockRepositoryManagement struct {
	ctrl     *gomock.Controller
	recorder *MockRepositoryManagementMockRecorder
}

// MockRepositoryManagementMockRecorder is the mock recorder for MockRepositoryManagement.
type MockRepositoryManagementMockRecorder struct {
	mock *MockRepositoryManagement
}

// NewMockRepositoryManagement creates a new mock instance.
func NewMockRepositoryManagement(ctrl *gomock.Controller) *MockRepositoryManagement {
	mock := &MockRepositoryManagement{ctrl: ctrl}
	mock.recorder = &MockRepositoryManagementMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockRepositoryManagement) EXPECT() *MockRepositoryManagementMockRecorder {
	return m.recorder
}

// CreateImage mocks base method.
func (m *MockRepositoryManagement) CreateImage(ctx context.Context, image interface{}) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateImage", ctx, image)
	ret0, _ := ret[0].(error)
	return ret0
}

// CreateImage indicates an expected call of CreateImage.
func (mr *MockRepositoryManagementMockRecorder) CreateImage(ctx, image interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateImage", reflect.TypeOf((*MockRepositoryManagement)(nil).CreateImage), ctx, image)
}

// DisableImage mocks base method.
func (m *MockRepositoryManagement) DisableImage(ctx context.Context, id string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DisableImage", ctx, id)
	ret0, _ := ret[0].(error)
	return ret0
}

// DisableImage indicates an expected call of DisableImage.
func (mr *MockRepositoryManagementMockRecorder) DisableImage(ctx, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DisableImage", reflect.TypeOf((*MockRepositoryManagement)(nil).DisableImage), ctx, id)
}

// EnableImage mocks base method.
func (m *MockRepositoryManagement) EnableImage(ctx context.Context, id string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "EnableImage", ctx, id)
	ret0, _ := ret[0].(error)
	return ret0
}

// EnableImage indicates an expected call of EnableImage.
func (mr *MockRepositoryManagementMockRecorder) EnableImage(ctx, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "EnableImage", reflect.TypeOf((*MockRepositoryManagement)(nil).EnableImage), ctx, id)
}

// GetByUUID mocks base method.
func (m *MockRepositoryManagement) GetByUUID(ctx context.Context, uuid string) (*v1.Image, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByUUID", ctx, uuid)
	ret0, _ := ret[0].(*v1.Image)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetByUUID indicates an expected call of GetByUUID.
func (mr *MockRepositoryManagementMockRecorder) GetByUUID(ctx, uuid interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByUUID", reflect.TypeOf((*MockRepositoryManagement)(nil).GetByUUID), ctx, uuid)
}

// GetImageById mocks base method.
func (m *MockRepositoryManagement) GetImageById(ctx context.Context, id string) (*v1.Image, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetImageById", ctx, id)
	ret0, _ := ret[0].(*v1.Image)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetImageById indicates an expected call of GetImageById.
func (mr *MockRepositoryManagementMockRecorder) GetImageById(ctx, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetImageById", reflect.TypeOf((*MockRepositoryManagement)(nil).GetImageById), ctx, id)
}

// GetImageWithPageInfo mocks base method.
func (m *MockRepositoryManagement) GetImageWithPageInfo(ctx context.Context, in interface{}) (*v1.ImagePage, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetImageWithPageInfo", ctx, in)
	ret0, _ := ret[0].(*v1.ImagePage)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetImageWithPageInfo indicates an expected call of GetImageWithPageInfo.
func (mr *MockRepositoryManagementMockRecorder) GetImageWithPageInfo(ctx, in interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetImageWithPageInfo", reflect.TypeOf((*MockRepositoryManagement)(nil).GetImageWithPageInfo), ctx, in)
}

// UpdateIcon mocks base method.
func (m *MockRepositoryManagement) UpdateIcon(ctx context.Context, id string, icon []byte) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateIcon", ctx, id, icon)
	ret0, _ := ret[0].(error)
	return ret0
}

// UpdateIcon indicates an expected call of UpdateIcon.
func (mr *MockRepositoryManagementMockRecorder) UpdateIcon(ctx, id, icon interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateIcon", reflect.TypeOf((*MockRepositoryManagement)(nil).UpdateIcon), ctx, id, icon)
}

// UpdateImage mocks base method.
func (m *MockRepositoryManagement) UpdateImage(ctx context.Context, data *v1.Image) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateImage", ctx, data)
	ret0, _ := ret[0].(error)
	return ret0
}

// UpdateImage indicates an expected call of UpdateImage.
func (mr *MockRepositoryManagementMockRecorder) UpdateImage(ctx, data interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateImage", reflect.TypeOf((*MockRepositoryManagement)(nil).UpdateImage), ctx, data)
}

// MockRepository is a mock of Repository interface.
type MockRepository struct {
	ctrl     *gomock.Controller
	recorder *MockRepositoryMockRecorder
}

// MockRepositoryMockRecorder is the mock recorder for MockRepository.
type MockRepositoryMockRecorder struct {
	mock *MockRepository
}

// NewMockRepository creates a new mock instance.
func NewMockRepository(ctrl *gomock.Controller) *MockRepository {
	mock := &MockRepository{ctrl: ctrl}
	mock.recorder = &MockRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockRepository) EXPECT() *MockRepositoryMockRecorder {
	return m.recorder
}

// GetImagesByEnabled mocks base method.
func (m *MockRepository) GetImagesByEnabled(ctx context.Context) ([]*v1.Image, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetImagesByEnabled", ctx)
	ret0, _ := ret[0].([]*v1.Image)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetImagesByEnabled indicates an expected call of GetImagesByEnabled.
func (mr *MockRepositoryMockRecorder) GetImagesByEnabled(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetImagesByEnabled", reflect.TypeOf((*MockRepository)(nil).GetImagesByEnabled), ctx)
}
